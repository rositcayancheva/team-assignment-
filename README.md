This is the team assignment project of Rossie and Ico. 

We've attempted (to some extend successfully) to design and implement a Work Item Management (WIM) Console Application.

Please have a look. 

TEST COMMANDS

create team/Slytherin
create board/Slytherin/MagicBoard
create person/ Draco Malfoy
create person/ Lord Voldemort
add person to team / Draco Malfoy/ Slytherin
add person to team / Lord Voldemort/ Slytherin
create bug/ Slytherin/ MagicBoard/ Draco Malfoy/666/ Black magic and other curses/ The spells need to be translated in English./ Active/ Find a dictionary. Find someone to translate the book. / High / Critical
change bug priority/ 666/ Draco Malfoy/ Low
change bug severity/ 666/ Draco Malfoy/ Minor
change bug status/ 666/ Draco Malfoy/ Fixed
create bug/ Slytherin/ MagicBoard/ Draco Malfoy/ 999/ Flying homework/ Problems with the flying broom/ Fixed/ Call the broom repair shop. Test the broom. /Medium / Critical
create bug/ Slytherin/ MagicBoard/ Draco Malfoy/ 492/ Full Moon in Leo/ Potions with red wine / Active/ Find wine from the cursed valley. Stir it clockwise. /Low / Minor
create story/ Slytherin/ MagicBoard/ Draco Malfoy/ 215/ Basilisk of Slytherin / Legend of the Serpent/ inProgress/ Low/ Small
create Feedback/Slytherin/ MagicBoard/Lord Voldemort/341/Evil plan progress/Fight Dumbledore in the tower /Done/ 9
list all team members / Slytherin
show board activity/ Slytherin/ MagicBoard
show personal activity/ Draco Malfoy
show team activity/ Slytherin
Exit

create person/ Morgan Freeman
create person / Clint Eastwood 
create team/ MorganTeam
create board/MorganTeam/ Awards
add person to team/ Morgan Freeman/ MorganTeam
add person to team/ Clint Eastwood/ MorganTeam
create story/ MorganTeam/ Awards/ Morgan Freeman/ 299/ Long Walk to Freedom / Autobiography of Nelson Mandela/ Done/ High/ Large
assign workitem/ Morgan Freeman/ 299
change story size/ 299/ Morgan Freeman/ Small
change story priority/ 299/ Morgan Freeman/ Medium
change story status/ 299/ Morgan Freeman/ NotDone
add comment / Morgan Freeman / 299 / “I don't want a Black History Month. Black history is American history.”
add comment / Clint Eastwood / 299 / “I always liked characters that were more grounded in reality.”
show board activity/ MorganTeam/ Awards
show personal activity/ Morgan Freeman
show team activity/ MorganTeam
list comments / 299
create bug/ MorganTeam/ Awards/ Morgan Freeman/ 333/OuchOuchOuch/ Dessssscrrription/ Fixed/ Step 1. Step 2. /Low / Major
unassign workitem/ 299
create bug/ MorganTeam/ Awards/ Morgan Freeman/ 553/Yyy-yyy-yyy/ Dessssscrrriptionnnn/ Active/ Step 1. Step 2. /High / Critical
create Feedback/MorganTeam /Awards/ Morgan Freeman/934/Teletubbies/LongDescriptionAgain/Scheduled/-2
Exit

create team/ Djakuzi team
create board/Djakuzi team/ Spa Oferta
create person/Kolio Ficheto
add person to team / Kolio Ficheto / Djakuzi team
create feedback/ Djakuzi team/ Spa Oferta/ Kolio Ficheto/ 998/ Djakuzi Feedback / Kolio Ficheto would have made the djakuzi better / Scheduled/ 9
change Feedback status/ 998/ Kolio Ficheto/ Unscheduled
change Feedback rating/ 998/ Kolio Ficheto/ 3
show board activity/ Djakuzi team/ Spa Oferta
show personal activity/ Kolio Ficheto
show team activity/ Djakuzi team
create person/ Bahari Zaharov
create person/ Kaka Mara
create person/ Kim Chen Un
add person to team/ Bahari Zaharov/ Djakuzi team
add person to team/ Kaka Mara/ Djakuzi team
add person to team/ Kim Chen Un/ Djakuzi team
list all team members/ Djakuzi team
add comment/ Kaka Mara/ 998/ Taka doide vdyhnovenieto za imenata prosto
create bug/ Djakuzi team/ Spa Oferta/ Kaka Mara/ 234 /GamaGamaGama/ Descrrrrrription/ Active/ Step 1. Step 2. /Medium / Major
assign workitem/ Kaka Mara/ 234
create story/ Djakuzi team/ Spa Oferta/ Kolio Ficheto/ 388/ Another day another story/ Descroption again/ NotDone/ Medium/ Small
list all/ workitems
list all/ bugs
list all/ feedbacks
list all/ stories
list all people
list all team boards / Djakuzi team
list all team members / Djakuzi team
list all teams
list by status/ Active
list by status/ Scheduled
sort by priority/ High
list by assignee / Kaka Mara
sort by rating
sort by size
Exit

TESTING EXCEPTIONS
create person/ Bob
Create person / Bobby
Create person / Bobby
create team/ ABCD
create team/ ABCDE
create team/ ABCDE
create board/ABCDE / A12345678910
create board / ABCDE / A12345678
create board / ABCDE / A12345678
add person to team / Bobby/ ABCDE
add person to team / Bobby/ ABCDE
create bug/ ABCDE/ A12345678/ Bobby /123/ This is a title / This is a description / Active/ Step one. Step two. / High / Critical
create bug/ ABCDE/ A12345678/ Bobby /123/ This is a title / This is a description / Active/ Step one. Step two. / High / Critical
Exit
