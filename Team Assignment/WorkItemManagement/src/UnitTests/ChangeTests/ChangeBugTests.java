package UnitTests.ChangeTests;

import com.telerikacademy.team_assignment.Commands.Creation.*;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeBugPriority;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeBugSeverity;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeBugStatus;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugTests {
    private static WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
    private static Engine engine = new EngineImpl(factory);
    private static List<String> parameters = new ArrayList<>();
    Person person = new PersonImpl("Gosheto");
    Team team = new TeamImpl("Timut na Gosho");
    Board board = new BoardImpl("Bordut");
    Bug bug = new BugImpl("223", "Bugut na Gosho", "Descriptionut na Gosho", "Active",
            "GOsho1. Gosho 2.", Priority.Low, Severity.Major);

    @Before
    public void setUp() {
        engine.getHashMapPeople().put("Gosheto", person);
        engine.getHashMapTeams().put("Timut na Gosho", team);
        team.addPersonToTeam(person);
        team.addBoardToTeam("Bordut", board);
        engine.getHashMapWorkItems().put("223", bug);
        engine.getHashMapTeams().get("Timut na Gosho").getHashMapBoards().get("Bordut").addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);
    }

    @Test
    public void changeBugPriorityShouldChangePriority(){
        ChangeBugPriority changeBugPriority = new ChangeBugPriority(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("High");
        changeBugPriority.execute(parameters);
        Assert.assertEquals(bug.getPriority(), Priority.High);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenPriorityIsWrong(){
        ChangeBugPriority changeBugPriority = new ChangeBugPriority(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("WrongPriority");
        changeBugPriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenItemIsNotABug(){
        //create a story
        Story story = new StoryImpl("111", "StoryTitle",
                "Description of the Story", "Done",
                Priority.High, StorySize.Large);
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapTeams().get("Timut na Gosho").getHashMapBoards().get("Bordut").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);

        //try to change priority of story
        ChangeBugPriority changeBugPriority = new ChangeBugPriority(factory, engine);
        parameters.add("111");
        parameters.add("Gosheto");
        parameters.add("Low");
        changeBugPriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugPriorityWithWrongIDShouldThrowException(){
        ChangeBugPriority changePriority = new ChangeBugPriority(factory, engine);
        parameters.add("WrongID");
        parameters.add("Gosheto");
        parameters.add("Critical");
        changePriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugPriorityShouldThrowExceptionWhenPersonDoesntExist(){
        ChangeBugPriority changePriority = new ChangeBugPriority(factory, engine);
        parameters.add("223");
        parameters.add("SantaClaus");
        parameters.add("Critical");
        changePriority.execute(parameters);
    }

    @Test
    public void changePriorityShouldAddToTheActivityHistory(){
        ChangeBugPriority changePriority = new ChangeBugPriority(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("Low");
        changePriority.execute(parameters);
        String example = "Gosheto changed the priority of bug 223 to Low" + "\n";
        Assert.assertEquals(example, team.getHashMapBoards().get(board.getName()).getActivityHistory().toString());
    }


    @Test
    public void changeBugSeverityShouldChange(){
        ChangeBugSeverity changeBugSeverity = new ChangeBugSeverity(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("Critical");
        changeBugSeverity.execute(parameters);
        Assert.assertEquals(bug.getSeverity(), Severity.Critical);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugSeverityWithIllegalSeverityShouldThrowException(){
        ChangeBugSeverity changeBugSeverity = new ChangeBugSeverity(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("Wrong Severity");
        changeBugSeverity.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugSeverityWithWrongIDShouldThrownException(){
        ChangeBugSeverity changeBugSeverity = new ChangeBugSeverity(factory, engine);
        parameters.add("WrongID");
        parameters.add("Gosheto");
        parameters.add("Critical");
        changeBugSeverity.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSeverityShouldThrowExceptionWhenItemIsNotABug(){
        //create a story
        Story story = new StoryImpl("111", "StoryTitle",
                "Description of the Story", "Done",
                Priority.High, StorySize.Large);
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapTeams().get("Timut na Gosho").getHashMapBoards().get("Bordut").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);

        //try change severity of a story
        ChangeBugSeverity changeSeverity = new ChangeBugSeverity(factory, engine);
        parameters.add("111");
        parameters.add("Gosheto");
        parameters.add("Low");
        changeSeverity.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSeverityWithUnexistingPersonShouldThrowException(){
        ChangeBugSeverity changeSeverity = new ChangeBugSeverity(factory, engine);
        parameters.add("223");
        parameters.add("Sandman");
        parameters.add("Critical");
        changeSeverity.execute(parameters);
    }

    @Test
    public void changeBugStatusShouldChangeBugStatus(){
        ChangeBugStatus changeStatus = new ChangeBugStatus(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("Fixed");
        changeStatus.execute(parameters);
        Assert.assertEquals("Fixed", bug.getStatus());
    }

    @Test (expected = IllegalArgumentException.class)
    public void changeBugStatusWithWrongStatusShouldThrowException(){
        ChangeBugStatus changeStatus = new ChangeBugStatus(factory, engine);
        parameters.add("223");
        parameters.add("Gosheto");
        parameters.add("WrongStatus");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugStatusWithWrongIDShouldThrowException(){
        ChangeBugStatus changeStatus = new ChangeBugStatus(factory, engine);
        parameters.add("000");
        parameters.add("Gosheto");
        parameters.add("Fixed");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugStatusShouldThrowExceptionWhenTheItemIsNotABug(){
        //create a story
        Story story = new StoryImpl("111", "StoryTitle",
                "Description of the Story", "Done",
                Priority.High, StorySize.Large);
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapTeams().get("Timut na Gosho").getHashMapBoards().get("Bordut").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);

        ChangeBugStatus changeStatus = new ChangeBugStatus(factory, engine);
        parameters.add("111");
        parameters.add("Gosheto");
        parameters.add("Fixed");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugStatusShouldThrowExceptionWhenPersonDoesNotExist(){
        ChangeBugStatus changeStatus = new ChangeBugStatus(factory, engine);
        parameters.add("223");
        parameters.add("Anonymous Dude");
        parameters.add("Fixed");
        changeStatus.execute(parameters);
    }
    
    @After
    public void clear(){
        parameters.clear();
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }
}
