package UnitTests.ChangeTests;

import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeBugPriority;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeStoryPriority;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeStorySize;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeStoryStatus;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStoryTests {
    private static WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
    private static Engine engine = new EngineImpl(factory);
    private static List<String> parameters = new ArrayList<>();
    Person person = new PersonImpl("Gandalf");
    Team team = new TeamImpl("TheFellowship");
    Board board = new BoardImpl("TheRing");
    Story story = new StoryImpl("999", "TheFellowship", "Descriptioooooooooon", "Done",
            Priority.Medium, StorySize.Large);

    @Before
    public void setUp() {
        engine.getHashMapPeople().put("Gandalf", person);
        engine.getHashMapTeams().put("TheFellowship", team);
        team.addPersonToTeam(person);
        team.addBoardToTeam("TheRing", board);
        engine.getHashMapWorkItems().put("999", story);
        engine.getHashMapTeams().get("TheFellowship").getHashMapBoards().get("TheRing").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);
    }

    @Test
    public void changePriorityShouldChangePriority(){
        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("High");
        changePriority.execute(parameters);
        Assert.assertEquals(story.getPriority(), Priority.High);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenPriorityIsInvalid(){
        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("InvalidPriority");
        changePriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenIDIsInvalid(){
        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("000");
        parameters.add("Gandalf");
        parameters.add("High");
        changePriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenPersonDoesNotExist(){
        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("999");
        parameters.add("Saruman");
        parameters.add("High");
        changePriority.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePriorityShouldThrowExceptionWhenItemIsNotAStory(){
        Bug bug = new BugImpl("223", "Bugut na Gandalf", "Descriptionut na Gandalf", "Active",
                "Point1. Point 2.", Priority.Low, Severity.Major);
        engine.getHashMapWorkItems().put("223", bug);
        engine.getHashMapTeams().get("TheFellowship").getHashMapBoards().get("TheRing").addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);

        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("223");
        parameters.add("Gandalf");
        parameters.add("High");
        changePriority.execute(parameters);
    }

    @Test
    public void changePriorityShouldAddToTheActivityHistory(){
        ChangeStoryPriority changePriority = new ChangeStoryPriority(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("High");
        changePriority.execute(parameters);
        String example = "Gandalf changed the priority of story 999 to High" + "\n";
        Assert.assertEquals(example, story.getActivityHistory().toString());
    }

    @Test
    public void changeStorySizeShouldChangeSize(){
        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("Small");
        changeSize.execute(parameters);
        Assert.assertEquals(StorySize.Small, story.getSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSizeShouldThrowExceptionWhenSizeIsInvalid(){
        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("InvalidSize");
        changeSize.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSizeShouldThrowExceptionWhenIDisInvalid(){
        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("000");
        parameters.add("Gandalf");
        parameters.add("Small");
        changeSize.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSizeShouldThrowExceptionWhenPersonDoesNotExist(){
        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("999");
        parameters.add("Gollum");
        parameters.add("Small");
        changeSize.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeSizeShouldThrowExceptionWhenItemIsNotAStory(){
        Bug bug = new BugImpl("223", "Bugut na Gandalf", "Descriptionut na Gandalf", "Active",
                "Point1. Point 2.", Priority.Low, Severity.Major);
        engine.getHashMapWorkItems().put("223", bug);
        engine.getHashMapTeams().get("TheFellowship").getHashMapBoards().get("TheRing").addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);

        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("223");
        parameters.add("Gandalf");
        parameters.add("Small");
        changeSize.execute(parameters);
    }

    @Test
    public void changeSizeShouldAddToTheActivityHistory(){
        ChangeStorySize changeSize = new ChangeStorySize(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("Small");
        changeSize.execute(parameters);
        String example = "Gandalf changed the size of story 999 to Small" + "\n";
        Assert.assertEquals(example, story.getActivityHistory().toString());
    }

    @Test
    public void changeStoryStatusShouldChangeStatus(){
        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("InProgress");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryStatusShouldThrowExceptionWhenStatusIsInvalid(){
        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("InvalidStatus");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryStatusShouldThrowExceptionWhenIDisInvalid(){
        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("000");
        parameters.add("Gandalf");
        parameters.add("NotDone");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryStatusShouldThrowExceptionWhenPersonDoesNotExist(){
        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("999");
        parameters.add("Frodo");
        parameters.add("NotDone");
        changeStatus.execute(parameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void changeStoryStatusShouldThrowExceptionWhenItemIsNotAStory(){
        Bug bug = new BugImpl("223", "Bugut na Gandalf", "Descriptionut na Gandalf", "Active",
                "Point1. Point 2.", Priority.Low, Severity.Major);
        engine.getHashMapWorkItems().put("223", bug);
        engine.getHashMapTeams().get("TheFellowship").getHashMapBoards().get("TheRing").addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);

        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("223");
        parameters.add("Gandalf");
        parameters.add("NotDone");
        changeStatus.execute(parameters);
    }
    @Test
    public void changeStatusShouldAddToTheActivityHistory(){
        ChangeStoryStatus changeStatus = new ChangeStoryStatus(factory, engine);
        parameters.add("999");
        parameters.add("Gandalf");
        parameters.add("NotDone");
        changeStatus.execute(parameters);
        String example = "Gandalf changed the status of story 999 to NotDone" + "\n";
        Assert.assertEquals(example, story.getActivityHistory().toString());
    }

    @After
    public void clear(){
        parameters.clear();
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }
}
