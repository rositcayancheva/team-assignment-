package UnitTests.ChangeTests;

import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeFeedbackRating;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.ChangeFeedbackStatus;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.FeedbackImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedbackTests {
    private static WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
    private static Engine engine = new EngineImpl(factory);
    private static List<String> parameters = new ArrayList<>();
    Person person = new PersonImpl("Stanka");
    Team team = new TeamImpl("StankaAndCo");
    Board board = new BoardImpl("StankaBo");
    Feedback feedback = new FeedbackImpl("222", "StankaFeed", "Stanka's Description",
            "Scheduled", 4);

    @Before
    public void setUp() {
        engine.getHashMapPeople().put("Stanka", person);
        engine.getHashMapTeams().put("StankaAndCo", team);
        team.addPersonToTeam(person);
        team.addBoardToTeam("StankaBo", board);
        engine.getHashMapWorkItems().put("222", feedback);
        engine.getHashMapTeams().get("StankaAndCo").getHashMapBoards().get("StankaBo").addWorkItem(feedback);
        feedback.setBoardOfWorkItem(board);
        feedback.setTeamOfWorkItem(team);
    }

    @Test
    public void changeFeedbackRatingShouldChangeRating(){
        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("7");
        changeRating.execute(parameters);
        Assert.assertEquals(feedback.getRating(), 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingShouldThrowExceptionWhenIDisInvalid(){
        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("232");
        parameters.add("Stanka");
        parameters.add("7");
        changeRating.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingShouldThrowExceptionWhenFailedToParseInt(){
        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("AAA");
        changeRating.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingShouldThrowExceptionWhenNameDoesNotExist(){
        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("222");
        parameters.add("Tzonka");
        parameters.add("7");
        changeRating.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingShouldThrowExceptionWhenItemIsNotAFeedback(){
        Story story = new StoryImpl("999", "SomeStoryTitle", "Descriptioooooooooon", "Done",
                Priority.Medium, StorySize.Large);
        engine.getHashMapWorkItems().put("999", story);
        engine.getHashMapTeams().get("StankaAndCo").getHashMapBoards().get("StankaBo").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);

        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("999");
        parameters.add("Stanka");
        parameters.add("7");
        changeRating.execute(parameters);
    }

    @Test
    public void changeRatingShouldAddToActivityHistory(){
        ChangeFeedbackRating changeRating = new ChangeFeedbackRating(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("7");
        changeRating.execute(parameters);
        String example = "Stanka changed the rating of feedback 222 to 7" + "\n";
        Assert.assertEquals(example, feedback.getActivityHistory().toString());
    }

    @Test
    public void changeFeedbackStatusShouldChangeStatus(){
        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("New");
        changeStatus.execute(parameters);
        Assert.assertEquals(feedback.getStatus(), "New");
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedbackStatusShouldThrowExceptionWhenIDisInvalid(){
        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("rrr");
        parameters.add("Stanka");
        parameters.add("New");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedbackStatusShouldThrowExceptionWhenStatusIsInvalid(){
        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("InvalidStatus");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedbackStatusShouldThrowExceptionWhenPersonDoesNotExist(){
        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("222");
        parameters.add("Totka");
        parameters.add("New");
        changeStatus.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedbackStatusShouldThrowExceptionWhenItemIsNotAFeedback(){
        Story story = new StoryImpl("999", "SomeStoryTitle", "Descriptioooooooooon", "Done",
                Priority.Medium, StorySize.Large);
        engine.getHashMapWorkItems().put("999", story);
        engine.getHashMapTeams().get("StankaAndCo").getHashMapBoards().get("StankaBo").addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);

        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("999");
        parameters.add("Stanka");
        parameters.add("New");
        changeStatus.execute(parameters);
    }

    @Test
    public void changeFeedbackStatusShouldAddToActivityHistory(){
        ChangeFeedbackStatus changeStatus = new ChangeFeedbackStatus(factory, engine);
        parameters.add("222");
        parameters.add("Stanka");
        parameters.add("New");
        changeStatus.execute(parameters);

        String example = "Stanka changed the status of feedback 222 to New" + "\n";
        Assert.assertEquals(example, feedback.getActivityHistory().toString());
    }

    @After
    public void clear(){
        parameters.clear();
    }
}
