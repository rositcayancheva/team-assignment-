package UnitTests.AssigningTest;

import com.telerikacademy.team_assignment.Commands.Operations.AssignWorkItem;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.FeedbackImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AssignWorkitemTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private Team team;
    private Person person;
    private Board board;

    private AssignWorkItem assignWorkitem;
    private List<String> parameters;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        board = new BoardImpl("Qk board");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        team.addPersonToTeam(person);
        team.addBoardToTeam(board.getName(), board);
        parameters = new ArrayList<>();

        assignWorkitem = new AssignWorkItem(factory, engine);
    }

    @After
    public void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }


    @Test
    public void assignWorkitemShouldAssignBugs() {
        //Arrange
        Bug bug = new BugImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn", "Active",
                "Step 1. Step 2.", Priority.High, Severity.Critical);
        engine.getHashMapWorkItems().put(bug.getID(), bug);
        board.addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);

        parameters.add(person.getName());
        parameters.add(bug.getID());

        //Act
        assignWorkitem.execute(parameters);

        //Assert
        Assert.assertSame(person, bug.getAssignee());
    }

    @Test
    public void assignWorkitemShouldAssignStories() {
        //Arrange
        Story story = new StoryImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn",
                "NotDone", Priority.Medium, StorySize.Small);

        engine.getHashMapWorkItems().put(story.getID(), story);
        board.addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);
        parameters.add(person.getName());
        parameters.add(story.getID());

        //Act
        assignWorkitem.execute(parameters);

        //Assert
        Assert.assertSame(person, story.getAssignee());
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignWorkitemShouldThrowExeptionForFeedBack() {
        Feedback feedback = new FeedbackImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn",
                "New", 2);
        engine.getHashMapWorkItems().put(feedback.getID(), feedback);
        board.addWorkItem(feedback);
        feedback.setBoardOfWorkItem(board);
        feedback.setTeamOfWorkItem(team);
        parameters.add(person.getName());
        parameters.add(feedback.getID());

        assignWorkitem.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignWorkitemShouldThrowExceptionWhenWorkitemIsAlreadyAssigned() {

        Story story = new StoryImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn",
                "NotDone", Priority.Medium, StorySize.Small);

        engine.getHashMapWorkItems().put(story.getID(), story);
        board.addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);
        parameters.add(person.getName());
        parameters.add(story.getID());

        Person anotherPerson = new PersonImpl("Intern");
        engine.getHashMapPeople().put(anotherPerson.getName(), anotherPerson);
        team.addPersonToTeam(anotherPerson);

        List<String> newParams = new ArrayList<>();
        newParams.add(anotherPerson.getName());
        newParams.add(story.getID());

        assignWorkitem.execute(parameters);
        assignWorkitem.execute(newParams);

    }

    @Test
    public void assignWorkitemShouldAddItemToPersonList (){
        Story story = new StoryImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn",
                "NotDone", Priority.Medium, StorySize.Small);

        engine.getHashMapWorkItems().put(story.getID(), story);
        board.addWorkItem(story);
        story.setBoardOfWorkItem(board);
        story.setTeamOfWorkItem(team);
        parameters.add(person.getName());
        parameters.add(story.getID());

        //Act
        assignWorkitem.execute(parameters);

        //Assert
        Assert.assertTrue(person.getWorkItems().contains(story));

    }

}
