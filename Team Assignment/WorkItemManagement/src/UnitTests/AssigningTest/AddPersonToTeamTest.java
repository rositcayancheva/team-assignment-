package UnitTests.AssigningTest;


import com.telerikacademy.team_assignment.Commands.Operations.AddPersonToTeam;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeamTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;
    private  Team team;
    private  Person person;
    private  List<String> parameters;
    private AddPersonToTeam addPersonToTeam;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        parameters = new ArrayList<>();

        addPersonToTeam = new AddPersonToTeam(factory, engine);
    }

    @After
    public void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
    }

    @Test
    public void addPersonToTeamShouldAct (){
        parameters.add(person.getName());
        parameters.add(team.getName());
        addPersonToTeam.execute(parameters);
        Assert.assertTrue(team.getMembers().contains(person));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addPersonToTeamShouldThrowExceptionWhenTeamDoesntExist (){
        parameters.add(person.getName());
        parameters.add("Not Qk Team");
        addPersonToTeam.execute(parameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void addPersonToTeamShouldThrowExceptionWhenPersonDoesntExist (){
        parameters.add("Not Qk Person");
        parameters.add(team.getName());
        addPersonToTeam.execute(parameters);
    }
}
