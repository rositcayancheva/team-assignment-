package UnitTests.AssigningTest;

import com.telerikacademy.team_assignment.Commands.Operations.AssignWorkItem;
import com.telerikacademy.team_assignment.Commands.Operations.UnassignWorkItem;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnassignWorkitemTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private Team team;
    private Person person;
    private Board board;
    private Bug bug;

    private UnassignWorkItem unassignWorkItem;
    private List<String> parameters;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        board = new BoardImpl("Qk board");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        team.addPersonToTeam(person);
        team.addBoardToTeam(board.getName(), board);
        parameters = new ArrayList<>();

        unassignWorkItem = new UnassignWorkItem(factory, engine);

        bug = new BugImpl("123", "AlphaAlphaAlpha", "Descriptionnnnnnn", "Active",
                "Step 1. Step 2.", Priority.High, Severity.Critical);
        engine.getHashMapWorkItems().put(bug.getID(), bug);
        board.addWorkItem(bug);
        bug.setBoardOfWorkItem(board);
        bug.setTeamOfWorkItem(team);

    }

    @After
    public void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }

    @Test
    public void unassignWorkitemShouldAct (){
        ((Assignable) bug).setAssignee(person);
        ((Assignable) bug).setAssigned(true);
        parameters.add(bug.getID());
        unassignWorkItem.execute(parameters);
        Assert.assertTrue(!(((Assignable)bug).isAssigned()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void unassignWorkitemShouldThrowExceptionIfWorkitemIsNotAssigned (){
        parameters.add(person.getName());
        parameters.add(bug.getID());
        unassignWorkItem.execute(parameters);
        Assert.assertTrue(!(((Assignable)bug).isAssigned()));
    }

    @Test
    public void unassignWorkitemShouldRemoveItemFromListOfPerson(){
        ((Assignable) bug).setAssignee(person);
        ((Assignable) bug).setAssigned(true);
        parameters.add(bug.getID());
        unassignWorkItem.execute(parameters);
        Assert.assertTrue(!(person.getWorkItems().contains(bug)));
    }

}
