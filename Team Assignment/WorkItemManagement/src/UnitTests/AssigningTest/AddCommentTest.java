package UnitTests.AssigningTest;

import com.telerikacademy.team_assignment.Commands.Creation.CreateStoryCommand;
import com.telerikacademy.team_assignment.Commands.Operations.AddComment;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddCommentTest {


    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private Team team;
    private Person person;
    private Board board;
    private List<String> parameters;

    private CreateStoryCommand createStoryCommand;
    private AddComment addComment;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        parameters = new ArrayList<>();

        createStoryCommand = new CreateStoryCommand(factory, engine);
        addComment = new AddComment(factory, engine);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        board = new BoardImpl("Qk board");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        team.addBoardToTeam(board.getName(), board);
        team.addPersonToTeam(person);

        String[] parms = "Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters);
        parameters.clear();
    }

    @After
    public void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }

    @Test
    public void addCommentShouldAct() {
        WorkItem story = engine.getHashMapWorkItems().get("123");
        parameters.add(person.getName());
        parameters.add(story.getID());
        parameters.add("Some qk comment here");
        addComment.execute(parameters);
        Assert.assertEquals("[Some qk comment here]", story.getComments().toString().trim());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCommentShouldThrowExpceptionIfCommentIsEmpty() {
        WorkItem story = engine.getHashMapWorkItems().get("123");
        parameters.add(person.getName());
        parameters.add(story.getID());
        parameters.add("");
        addComment.execute(parameters);
    }

    @Test
    public void addCommentShouldAddToActivityHistory() {
        WorkItem story = engine.getHashMapWorkItems().get("123");
        parameters.add(person.getName());
        parameters.add(story.getID());
        parameters.add("Some qk comment here");
        addComment.execute(parameters);
        Assert.assertTrue(board.getActivityHistory().toString().contains("Qk person added a comment to Workitem with ID 123."));
    }

    @Test
    public void addCommentShouldSaveAuthor() {
        WorkItem story = engine.getHashMapWorkItems().get("123");
        parameters.add(person.getName());
        parameters.add(story.getID());
        parameters.add("Some qk comment here");
        addComment.execute(parameters);
        Assert.assertSame(story.getComments().get(0).getAuthor(), person);

    }
}
