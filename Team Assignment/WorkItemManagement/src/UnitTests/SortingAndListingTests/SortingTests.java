package UnitTests.SortingAndListingTests;

import com.telerikacademy.team_assignment.Commands.Operations.Listing.*;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.FeedbackImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortingTests {
    private static WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
    private static Engine engine = new EngineImpl(factory);
    private static List<String> parameters = new ArrayList<>();

    Story story = new StoryImpl("111", "Zzzzzzzzzzzz", "Descriptiooooooooon",
            "Done", Priority.Medium, StorySize.Large);
    Story story1 = new StoryImpl("222", "Yyyyyyyyyyyy", "Descriptiooonnnnn",
            "Done", Priority.Low, StorySize.Medium);
    Story story2 = new StoryImpl("333", "Aaaaaaaaaaaa", "Descriptiooonnnnn", "NotDone",
            Priority.High, StorySize.Large);

    Bug bug = new BugImpl("444", "Xxxxxxxxxxxxx", "Descriptionnnnnnnnn",
            "Active", "Step1. Step2. Step3.", Priority.Low, Severity.Major);
    Bug bug1 = new BugImpl("555", "Bbbbbbbbbbbbb", "Descriptionnnnnnnnn",
            "Fixed", "Step1. Step2. Step3.", Priority.High, Severity.Critical);
    Bug bug2 = new BugImpl("666", "Cccccccccccc", "Descriptionnnnnnnnn", "Active",
            "Step1. Step2. Step3.", Priority.Low, Severity.Major);

    Feedback feed = new FeedbackImpl("777", "Ddddddddddddd", "DescriptionFeedback",
            "Scheduled", 8);
    Feedback feed1 = new FeedbackImpl("888", "Eeeeeeeeeeeee", "DescriptionFeedback",
            "Unscheduled", 3);
    Feedback feed2 = new FeedbackImpl("999", "Ffffffffffff", "DescriptionFeedback",
            "Scheduled", -5);


    @Test
    public void sortByTitleShouldSort(){
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapWorkItems().put("222", story1);
        engine.getHashMapWorkItems().put("444", bug);

        SortByTitle sortByTitle = new SortByTitle(factory, engine);
        sortByTitle.execute(parameters);
        String sortedByTitle = sortByTitle.getStringBuilder();
        String rightString = "Bug ---\n" +
                "Title: Xxxxxxxxxxxxx\n" +
                "ID: 444\n" +
                "Status: Active\n" +
                "Priority: Low\n" +
                "Severity: Major\n" +
                "\n" +
                "Story ---\n" +
                "Title: Yyyyyyyyyyyy\n" +
                "ID: 222\n" +
                "Status: Done\n" +
                "Priority: Low\n" +
                "Size: Medium\n" +
                "\n" +
                "Story ---\n" +
                "Title: Zzzzzzzzzzzz\n" +
                "ID: 111\n" +
                "Status: Done\n" +
                "Priority: Medium\n" +
                "Size: Large";
        Assert.assertEquals(rightString.trim(), sortedByTitle.trim());
    }
    @Test
    public void sortByTitleShouldReturnNoItems(){

        SortByTitle sortByTitle = new SortByTitle(factory, engine);
        sortByTitle.execute(parameters);
        String sortedByTitle = sortByTitle.getStringBuilder();
        String rightString = "No workitems to show!";
        Assert.assertEquals(rightString.trim(), sortedByTitle.trim());
    }

    @Test
    public void sortBySizeShouldReturnSorted(){
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapWorkItems().put("222", story1);
        engine.getHashMapWorkItems().put("333", story2);

        SortBySize sortBySize = new SortBySize(factory, engine);
        sortBySize.execute(parameters);

        String sortedBySize = sortBySize.getSortedBySize();
        String correctSorting = "Story ---\n" +
                "Title: Zzzzzzzzzzzz\n" +
                "ID: 111\n" +
                "Status: Done\n" +
                "Priority: Medium\n" +
                "Size: Large\n" +
                "\n" +
                "Story ---\n" +
                "Title: Aaaaaaaaaaaa\n" +
                "ID: 333\n" +
                "Status: NotDone\n" +
                "Priority: High\n" +
                "Size: Large\n" +
                "\n" +
                "Story ---\n" +
                "Title: Yyyyyyyyyyyy\n" +
                "ID: 222\n" +
                "Status: Done\n" +
                "Priority: Low\n" +
                "Size: Medium";
        Assert.assertEquals(sortedBySize.trim(), correctSorting.trim());
    }
    @Test
    public void sortBySizeShouldReturnNoItems(){

        SortBySize sortBySize = new SortBySize(factory, engine);
        sortBySize.execute(parameters);
        String sortedBySize = sortBySize.getSortedBySize();
        String rightString = "There are no created stories to list";
        Assert.assertEquals(rightString.trim(), sortedBySize.trim());
    }

    @Test
    public void SortBySeverityShouldReturnSorted(){
        engine.getHashMapWorkItems().put("444", bug);
        engine.getHashMapWorkItems().put("555", bug1);
        engine.getHashMapWorkItems().put("666", bug2);

        SortBySeverity sortBySeverity = new SortBySeverity(factory, engine);
        sortBySeverity.execute(parameters);
        String sortedBySeverity = sortBySeverity.getSortedBySeverity();
        String correctSorting = "Bug ---\n" +
                "Title: Bbbbbbbbbbbbb\n" +
                "ID: 555\n" +
                "Status: Fixed\n" +
                "Priority: High\n" +
                "Severity: Critical\n" +
                "\n" +
                "Bug ---\n" +
                "Title: Xxxxxxxxxxxxx\n" +
                "ID: 444\n" +
                "Status: Active\n" +
                "Priority: Low\n" +
                "Severity: Major\n" +
                "\n" +
                "Bug ---\n" +
                "Title: Cccccccccccc\n" +
                "ID: 666\n" +
                "Status: Active\n" +
                "Priority: Low\n" +
                "Severity: Major";
        Assert.assertEquals(sortedBySeverity.trim(), correctSorting.trim());
    }

    @Test
    public void sortedBySeverityShouldReturnNoItems(){
        SortBySeverity sortBySeverity = new SortBySeverity(factory, engine);
        sortBySeverity.execute(parameters);
        String sortedBySeverity = sortBySeverity.getSortedBySeverity();
        String corectedMessage = "There are no created stories to list";
        Assert.assertEquals(sortedBySeverity, corectedMessage);
    }

    @Test
    public void sortedByRatingShouldReturnSorted(){
        engine.getHashMapWorkItems().put("777", feed);
        engine.getHashMapWorkItems().put("888", feed1);
        engine.getHashMapWorkItems().put("999", feed2);

        SortByRating sortByRating = new SortByRating(factory, engine);
        sortByRating.execute(parameters);
        String sortedByRating = sortByRating.getSortedByRating();
        String correctSorting = "Feedback ---\n" +
                "Title: Ffffffffffff\n" +
                "ID: 999\n" +
                "Status: Scheduled\n" +
                "Rating: -5\n" +
                "\n" +
                "Feedback ---\n" +
                "Title: Eeeeeeeeeeeee\n" +
                "ID: 888\n" +
                "Status: Unscheduled\n" +
                "Rating: 3\n" +
                "\n" +
                "Feedback ---\n" +
                "Title: Ddddddddddddd\n" +
                "ID: 777\n" +
                "Status: Scheduled\n" +
                "Rating: 8";
        Assert.assertEquals(sortedByRating.trim(), correctSorting.trim());
    }

    @Test
    public void sortByRatingShouldReturnNoItems(){
        SortByRating sortByRating = new SortByRating(factory, engine);
        sortByRating.execute(parameters);
        String sortedByRating = sortByRating.getSortedByRating();
        String correctSorting = "There are no WorkItems to list";
        Assert.assertEquals(sortedByRating.trim(), correctSorting.trim());
    }
    @Test
    public void sortByPriorityShouldReturnSorted(){
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapWorkItems().put("444", bug);
        engine.getHashMapWorkItems().put("555", bug1);

        SortByPriority sortByPriority = new SortByPriority(factory, engine);
        sortByPriority.execute(parameters);

        String sortedByPriority = sortByPriority.getSortedByPriority();
        String correctSorting = "Bugs: \n" +
                "Bug ---\n" +
                "Title: Bbbbbbbbbbbbb\n" +
                "ID: 555\n" +
                "Status: Fixed\n" +
                "Priority: High\n" +
                "Severity: Critical\n" +
                "\n" +
                "Bug ---\n" +
                "Title: Xxxxxxxxxxxxx\n" +
                "ID: 444\n" +
                "Status: Active\n" +
                "Priority: Low\n" +
                "Severity: Major\n" +
                "\n" +
                "Stories: \n" +
                "Story ---\n" +
                "Title: Zzzzzzzzzzzz\n" +
                "ID: 111\n" +
                "Status: Done\n" +
                "Priority: Medium\n" +
                "Size: Large";

        Assert.assertEquals(sortedByPriority.trim(), correctSorting.trim());
    }

    @Test
    public void sortByPriorityShouldReturnNoItems(){
        SortByPriority sortByPriority = new SortByPriority(factory, engine);
        sortByPriority.execute(parameters);

        String sortedByPriority = sortByPriority.getSortedByPriority();
        String correctSorting = "There are no WorkItems to list";
        Assert.assertEquals(sortedByPriority, correctSorting);
    }

    @Test
    public void sortByStatusShouldReturnSorted(){
        engine.getHashMapWorkItems().put("111", story);
        engine.getHashMapWorkItems().put("222", story1);
        engine.getHashMapWorkItems().put("333", story2);

        parameters.add("Done");
        ListByStatus listByStatus = new ListByStatus(factory, engine);
        listByStatus.execute(parameters);

        String listedByStatus = listByStatus.getListedByStatus();
        String correctListing = "Story ---\n" +
                "Title: Zzzzzzzzzzzz\n" +
                "ID: 111\n" +
                "Status: Done\n" +
                "Priority: Medium\n" +
                "Size: Large\n" +
                "\n" +
                "Story ---\n" +
                "Title: Yyyyyyyyyyyy\n" +
                "ID: 222\n" +
                "Status: Done\n" +
                "Priority: Low\n" +
                "Size: Medium";
        Assert.assertEquals(listedByStatus.trim(), correctListing.trim());
    }

    @Test
    public void listByStatusShouldReturnNoItems(){
        parameters.add("Done");
        ListByStatus listByStatus = new ListByStatus(factory, engine);
        listByStatus.execute(parameters);
        String listedByStatus = listByStatus.getListedByStatus();
        String correctListing = String.format("There are no workitems with %s status to list", listByStatus.getWhichStatusToList());
        Assert.assertEquals(listedByStatus, correctListing);
    }

    @After
    public void clear(){
        engine.getHashMapWorkItems().clear();
        parameters.clear();
    }


//
//    @Test
//    public void printList(){
//
//        engine.getHashMapWorkItems().put("111", story);
//        engine.getHashMapWorkItems().put("222", story1);
//        engine.getHashMapWorkItems().put("333", story2);
//
//        ListByStatus listByStatus = new ListByStatus(factory, engine);
//        parameters.add("Done");
//        listByStatus.execute(parameters);
//
//        String listedByStatus = listByStatus.getListedByStatus();
//
//        System.out.println(listedByStatus);
//
//    }

}
