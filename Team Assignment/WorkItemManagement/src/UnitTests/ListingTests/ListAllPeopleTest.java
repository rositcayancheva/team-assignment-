package UnitTests.ListingTests;

import com.telerikacademy.team_assignment.Commands.Operations.Listing.ListAllPeople;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListAllPeopleTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private List<String> parameters ;
    private ListAllPeople listAllPeople;


    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        parameters = new ArrayList<>();
        listAllPeople = new ListAllPeople(factory, engine);
    }

    @After
    public void clenUp() {
        engine.getHashMapPeople().clear();
        parameters.clear();
    }

    @Test
    public void listAllPeopleShould() {
        Person firstPerson = new PersonImpl("Aadam");
        Person secondPerson = new PersonImpl("Eevvve");
        engine.getHashMapPeople().put(firstPerson.getName(), firstPerson);
        engine.getHashMapPeople().put(secondPerson.getName(), secondPerson);

        listAllPeople.execute(parameters);
        Assert.assertEquals("People created so far:\n" +
                "Aadam\n" +
                "Eevvve", listAllPeople.getStringBuilder().toString().trim());

    }

  @Test
  public void listAllPeopleShouldDisplayNoPeopleMessage(){
        listAllPeople.execute(parameters);
      Assert.assertEquals("There are no people to show!", listAllPeople.getStringBuilder().toString().trim());
  }
  }

