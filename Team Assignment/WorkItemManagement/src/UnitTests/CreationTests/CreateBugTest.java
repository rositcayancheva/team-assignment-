package UnitTests.CreationTests;


import com.telerikacademy.team_assignment.Commands.Creation.CreateBugTestCommand;
import com.telerikacademy.team_assignment.Commands.Operations.Listing.SortByTitle;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateBugTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private Team team;
    private Person person;
    private Board board;
    private  List<String> parameters;

    private static CreateBugTestCommand createBugTestCommand;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);

        createBugTestCommand = new CreateBugTestCommand(factory, engine);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        board = new BoardImpl("Qk board");
        team.addBoardToTeam(board.getName(), board);
        team.addPersonToTeam(person);
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        parameters = new ArrayList<>();
    }

    @After
    public  void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }

    @Test
    public void createBugShouldCreate() {
        String [] parms = "Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/Active/Step 1. Step 2./High/Critical".split("/");
        parameters.addAll(Arrays.asList(parms));
        createBugTestCommand.execute(parameters);
       Assert.assertTrue(engine.getHashMapWorkItems().containsKey("123"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBugShouldThrowExceptionWhenTeamDoesNotExist (){
        List<String> parameters = new ArrayList<>();
        String [] parms = "Not Qk team/Qk board/Qk person/123/Alpha Alpha Alpha/Description Description/Active/Step 1. Step 2./High/Critical".split("/");
        parameters.addAll(Arrays.asList(parms));
        createBugTestCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBugShouldThrowExceptionWhenBoardDoesNotExist(){
        List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Not Qk board/Qk person/123/Alpha Alpha Alpha/Description Description/Active/Step 1. Step 2./High/Critical".split("/");
        parameters.addAll(Arrays.asList(parms));
        createBugTestCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBugShouldThrowExceptionWhenPersonDoesNotExist(){
        List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Qk board/Not Qk person/123/Alpha Alpha Alpha/Description Description/Active/Step 1. Step 2./High/Critical".split("/");
        parameters.addAll(Arrays.asList(parms));
        createBugTestCommand.execute(parameters); }

    @Test
    public void createBugShouldAddMessageToActivityHistory(){
        List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Qk board/Qk person/123/Alpha Alpha Alpha/Description Description/Active/Step 1. Step 2./High/Critical".split("/");
        parameters.addAll(Arrays.asList(parms));
        createBugTestCommand.execute(parameters);
        Assert.assertEquals("Qk person created bug with ID: 123 in board Qk board.\n".trim(), engine.getHashMapWorkItems().get("123").getActivityHistory().toString().trim());
    }


    public void  testtes(){
      SortByTitle sortByTitle =  new SortByTitle( factory, engine );
      Assert.assertTrue(sortByTitle.getStringBuilder().equals(" kdskjfbs"));
    }


}
