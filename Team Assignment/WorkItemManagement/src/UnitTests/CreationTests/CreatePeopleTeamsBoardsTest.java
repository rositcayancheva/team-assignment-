package UnitTests.CreationTests;

import com.telerikacademy.team_assignment.Commands.Creation.CreateBoardCommand;
import com.telerikacademy.team_assignment.Commands.Creation.CreatePersonCommand;
import com.telerikacademy.team_assignment.Commands.Creation.CreateTeamCommand;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

public class CreatePeopleTeamsBoardsTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;
    private Team team;
    private Person person;
    private CreatePersonCommand createPersonCommand;
    private CreateBoardCommand createBoardCommand;
    private CreateTeamCommand createTeamCommand;
    private List<String> parameters;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createPersonCommand = new CreatePersonCommand(factory, engine);
        createBoardCommand = new CreateBoardCommand(factory, engine);
        createTeamCommand = new CreateTeamCommand(factory, engine);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        parameters = new ArrayList<>();
    }

    @After
    public void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
    }

    @Test
    public void createPersonCommandShouldCreate() {
        parameters.add("Che Guevara");
        createPersonCommand.execute(parameters);
        Assert.assertTrue(engine.getHashMapPeople().containsKey("Che Guevara"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPersonShouldThrowWxceptionWhenNameIsNotUnique() {
        parameters.add("Qk person");
        createPersonCommand.execute(parameters);
    }

    @Test
    public void createBoardCommandShouldCreate() {
        parameters.add("Qk team");
        parameters.add("One board");
        createBoardCommand.execute(parameters);
        Assert.assertTrue(team.getHashMapBoards().containsKey(parameters.get(1)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoardShouldThrowExeptionIfNameIsNotUnique() {
        parameters.add(team.getName());
        parameters.add("Qk board");
        createBoardCommand.execute(parameters);

        List<String> parametersSameBoardName = new ArrayList<>();
        parametersSameBoardName.add(team.getName());
        parametersSameBoardName.add("Qk board");
        createBoardCommand.execute(parametersSameBoardName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoardShouldThrowExceptionWhenTeamDoesntExist() {
        parameters.add("Not valid team");
        parameters.add("A Board");
        createBoardCommand.execute(parameters);
    }

    @Test
    public void createTeamShouldCreate() {
        parameters.add("Super team");
        createTeamCommand.execute(parameters);
        Assert.assertTrue(engine.getHashMapTeams().containsKey(parameters.get(0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createTeamShouldThrowExceptionWhenNameIsNotUnique() {
        parameters.add("Qk team");
        createTeamCommand.execute(parameters);
    }

    @Test
    public void createBoardShouldAddMessageToActivityHistory() {
        parameters.add(team.getName());
        parameters.add("Board 2.0");
        createBoardCommand.execute(parameters);
        Assert.assertSame("No activity to show", team.getHashMapBoards().get(parameters.get(1)).getActivityHistory().toString());
    }


}
