package UnitTests.CreationTests;

import com.telerikacademy.team_assignment.Commands.Creation.CreateStoryCommand;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateStoryTest {

    private static WorkItemManagementFactory factory;
    private static Engine engine;

    private Team team;
    private Person person;
    private Board board;
    private List<String> parameters;

    private CreateStoryCommand createStoryCommand;

    @Before
    public void setUp() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        parameters = new ArrayList<>();

        createStoryCommand = new CreateStoryCommand(factory, engine);

        team = new TeamImpl("Qk team");
        person = new PersonImpl("Qk person");
        board = new BoardImpl("Qk board");
        engine.getHashMapTeams().put(team.getName(), team);
        engine.getHashMapPeople().put(person.getName(), person);
        team.addBoardToTeam(board.getName(), board);
        team.addPersonToTeam(person);
    }

    @After
    public  void reset() {
        engine.getHashMapTeams().clear();
        engine.getHashMapPeople().clear();
        engine.getHashMapWorkItems().clear();
    }

    @Test
    public void createStoryShould () {
        String [] parms = "Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters);
        Assert.assertTrue(engine.getHashMapWorkItems().containsKey("123"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStoryShouldThrowExceptionWhenTeamDoesNotExist (){
        //List<String> parameters = new ArrayList<>();
        String [] parms = "Not Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStoryShouldThrowExceptionWhenBoardDoesNotExist(){
       // List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Not Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStoryShouldThrowExceptionWhenPersonDoesNotExist(){
        List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Qk board/Not Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters); }

    @Test(expected = IllegalArgumentException.class)
    public void createStoryShouldThrowExceptionWhenStatusIsInvalid(){
      //  List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/Active/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters); }

    @Test
    public void createStoryShouldAddMessageToActivityHistory(){
      //  List<String> parameters = new ArrayList<>();
        String [] parms = "Qk team/Qk board/Qk person/123/AlphaAlphaAlpha/Descriptionnnnnnn/NotDone/Medium/Small".split("/");
        parameters.addAll(Arrays.asList(parms));
        createStoryCommand.execute(parameters);
        Assert.assertEquals("Qk person created story with ID: 123 in board Qk board.\n".trim(), engine.getHashMapWorkItems().get("123").getActivityHistory().toString().trim());
    }
}
