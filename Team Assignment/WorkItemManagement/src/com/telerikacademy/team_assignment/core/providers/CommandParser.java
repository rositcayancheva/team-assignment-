package com.telerikacademy.team_assignment.core.providers;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Creation.*;
import com.telerikacademy.team_assignment.Commands.Operations.*;
import com.telerikacademy.team_assignment.Commands.Operations.Changes.*;
import com.telerikacademy.team_assignment.Commands.Operations.Listing.*;
import com.telerikacademy.team_assignment.Commands.Operations.ShowActivity.ShowBoardActivity;
import com.telerikacademy.team_assignment.Commands.Operations.ShowActivity.ShowPersonalActivity;
import com.telerikacademy.team_assignment.Commands.Operations.ShowActivity.ShowTeamActivity;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.contracts.Parser;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.ArrayList;
import java.util.List;

public class CommandParser implements Parser {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private final WorkItemManagementFactory factory;
    private final Engine engine;

    public CommandParser(WorkItemManagementFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split("/")[0].trim();
        return findCommand(commandName);
    }

    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split("/");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i].trim());
        }
        return parameters;
    }

    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            case "create person":
                return new CreatePersonCommand(factory, engine);

            case "create team":
                return new CreateTeamCommand(factory, engine);

            case "add person to team":
                return new AddPersonToTeam(factory, engine);

            case "add comment":
                return new AddComment(factory, engine);

            case "list all people":
                return new ListAllPeople(factory, engine);

            case "create board":
                return new CreateBoardCommand(factory, engine);

            case "list all team members":
                return new ListAllTeamMembersCommand(factory, engine);

            case "list all team boards":
                return new ListAllTeamBoards(factory, engine);

            case "list all teams":
                return new ListAllTeams(factory, engine);

            case "list comments":
                return new ListWorkitemComments(factory, engine);

            case "assign workitem":
                return new AssignWorkItem(factory, engine);

            case "show personal activity":
                return new ShowPersonalActivity(factory, engine);

            case "show team activity":
                return new ShowTeamActivity(factory, engine);

            case "show board activity":
                return new ShowBoardActivity(factory, engine);

            case "create bug":
                return new CreateBugTestCommand(factory, engine);

            case "change bug priority":
                return new ChangeBugPriority(factory, engine);

            case "change bug severity":
                return new ChangeBugSeverity(factory, engine);

            case "change bug status":
                return new ChangeBugStatus(factory, engine);

            case "create story":
                return new CreateStoryCommand(factory, engine);

            case "change story priority":
                return new ChangeStoryPriority(factory, engine);

            case "change story size":
                return new ChangeStorySize(factory, engine);

            case "change story status":
                return new ChangeStoryStatus(factory, engine);

            case "create feedback":
                return new CreateFeedbackCommand(factory, engine);

            case "change feedback status":
                return new ChangeFeedbackStatus(factory, engine);

            case "change feedback rating":
                return new ChangeFeedbackRating(factory, engine);

            case "list all":
                return new ListAll(factory, engine);

            case "list by status":
                return new ListByStatus(factory, engine);

            case "list by assignee":
                return new ListByAssignee(factory, engine);

            case "sort by title":
                return new SortByTitle(factory, engine);

            case "sort by priority":
                return new SortByPriority(factory, engine);

            case "sort by severity":
                return new SortBySeverity(factory, engine);

            case "sort by size":
                return new SortBySize(factory, engine);

            case "sort by rating":
                return new SortByRating(factory, engine);

            case "unassign workitem":
                return new UnassignWorkItem(factory, engine);

            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
        }
    }
}
