package com.telerikacademy.team_assignment.core.contracts;

public interface Reader {
    String readLine();
}
