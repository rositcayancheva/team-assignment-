package com.telerikacademy.team_assignment.core.contracts;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;

import java.util.HashMap;


public interface Engine {
    void start();

    HashMap<String, Person> getHashMapPeople();

    HashMap<String, Team> getHashMapTeams();

    HashMap<String, WorkItem> getHashMapWorkItems();

}
