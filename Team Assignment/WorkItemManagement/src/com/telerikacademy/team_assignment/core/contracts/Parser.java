package com.telerikacademy.team_assignment.core.contracts;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;

import java.util.List;

public interface Parser {
    Command parseCommand(String fullCommand);

    List<String> parseParameters(String fullCommand);
}
