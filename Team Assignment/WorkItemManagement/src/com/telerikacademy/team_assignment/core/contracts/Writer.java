package com.telerikacademy.team_assignment.core.contracts;

public interface Writer {
    void write(String message);

    void writeLine(String message);
}
