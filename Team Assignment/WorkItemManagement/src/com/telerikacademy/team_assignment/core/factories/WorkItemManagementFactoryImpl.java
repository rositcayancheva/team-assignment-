package com.telerikacademy.team_assignment.core.factories;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.BoardImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.TeamImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.BugImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.FeedbackImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.StoryImpl;

public class WorkItemManagementFactoryImpl implements WorkItemManagementFactory {
    public WorkItemManagementFactoryImpl() {
    }

    @Override
    public Person createPerson(String name) {
        return new PersonImpl(name);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String ID, String title, String description, String status, String stepsToreproduce, Priority priority, Severity severity) {
        return new BugImpl(ID, title, description, status, stepsToreproduce,  priority, severity);
    }

    @Override
    public Story createStory(String ID, String title, String description, String status, Priority priority, StorySize size) {
        return new StoryImpl(ID, title, description, status, priority, size);
    }

    @Override
    public Feedback createFeedback(String ID, String title, String description, String status, int rating) {
        return new FeedbackImpl(ID, title, description, status, rating);
    }
}
