package com.telerikacademy.team_assignment.core.factories;


import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;

public interface WorkItemManagementFactory {

    Person createPerson(String name);

    Team createTeam (String name);

    Board createBoard (String name);

    Bug createBug(String ID, String title, String description, String status, String stepsToreproduce ,Priority priority, Severity severity);

    Story createStory(String ID, String title, String description, String status, Priority priority, StorySize size);

    Feedback createFeedback(String ID, String title, String description, String status, int rating);
}
