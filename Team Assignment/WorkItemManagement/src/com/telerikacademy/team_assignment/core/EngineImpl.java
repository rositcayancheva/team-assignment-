package com.telerikacademy.team_assignment.core;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.contracts.Parser;
import com.telerikacademy.team_assignment.core.contracts.Reader;
import com.telerikacademy.team_assignment.core.contracts.Writer;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.providers.CommandParser;
import com.telerikacademy.team_assignment.core.providers.ConsoleReader;
import com.telerikacademy.team_assignment.core.providers.ConsoleWriter;

import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private Reader reader;
    private Writer writer;
    private Parser parser;

    private final HashMap<String, Person> hashMapPeople;
    private final HashMap<String, Team> hashMapTeams;
    private final HashMap<String, WorkItem> hashMapWorkItems;

    public EngineImpl(WorkItemManagementFactory factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        hashMapPeople = new HashMap<>();
        hashMapTeams = new HashMap<>();
        hashMapWorkItems = new HashMap<>();
    }

    @Override
    public HashMap<String, Team> getHashMapTeams() {
        return hashMapTeams;
    }

    @Override
    public HashMap<String, Person> getHashMapPeople() {
        return hashMapPeople;
    }

    @Override
    public HashMap<String, WorkItem> getHashMapWorkItems() {
        return hashMapWorkItems;
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                writer.writeLine("");
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        writer.writeLine("");
        Command command = parser.parseCommand(commandAsString);
        List<String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult.trim());

    }



}
