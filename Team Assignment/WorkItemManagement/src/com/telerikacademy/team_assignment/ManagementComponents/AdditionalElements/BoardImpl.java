package com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class BoardImpl implements Board {

    private final static int MIN_NAME_LEN = 5;
    private final static int MAX_NAME_LEN = 10;

    private String name;
    private List<WorkItem> listWorkItem;
    private ActivityHistory activityHistory;


    public BoardImpl(String name) {
        setName(name);
        listWorkItem = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
    }

    private void setName(String name) {
        if (!isNameValid.test(name)) {
            throw new IllegalArgumentException("Name should be between 5 and 10 symbols");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<WorkItem> getListWorkItem() {
        return new ArrayList<>(listWorkItem);
    }

    public void addWorkItem(WorkItem item){
        listWorkItem.add(item);
    }

    @Override
    public void addToActivityHistory(String string) {
        activityHistory.addToActivityHistory(string);
    }

    @Override
    public ActivityHistory getActivityHistory() {
        return activityHistory;
    }

    public String toString() {
        return getName();
    }

    private Predicate<String> isNameValid = (s) -> s.length() >= MIN_NAME_LEN && s.length() <= MAX_NAME_LEN;

}
