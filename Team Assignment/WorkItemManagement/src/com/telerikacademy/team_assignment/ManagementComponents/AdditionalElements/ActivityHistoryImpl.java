package com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;

import java.util.ArrayList;
import java.util.List;

public class ActivityHistoryImpl implements ActivityHistory {

    private List<String> activityHistory;

    public ActivityHistoryImpl() {
        activityHistory = new ArrayList<>();
    }

    public void addToActivityHistory(String string) {
        if (string.isEmpty()) {
            throw new IllegalArgumentException("Activity history cannot be an empty string");
        }
        activityHistory.add(string);
    }

    @Override
    public ActivityHistory getActivityHistory() {
        return this;
    }

    public String toString() {
        if (activityHistory.isEmpty()) {
            return "No activity to show";
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (String s : activityHistory) {
            stringBuilder.append(s);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

}
