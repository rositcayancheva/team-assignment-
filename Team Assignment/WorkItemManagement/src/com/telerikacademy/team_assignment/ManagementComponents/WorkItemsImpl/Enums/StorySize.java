package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums;

public enum StorySize {
    Large,
    Medium,
    Small
}
