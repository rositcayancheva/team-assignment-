package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;


public class Comment {
    private String message;
    private Person author;

    public Comment(String message, Person author) {
        setMessage(message);
        setAuthor(author);
    }

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return getMessage();
    }

    private void setMessage(String message) {
        if (message.isEmpty()) {
            throw new IllegalArgumentException("Message should not be empty");
        }
        this.message = message;
    }
}
