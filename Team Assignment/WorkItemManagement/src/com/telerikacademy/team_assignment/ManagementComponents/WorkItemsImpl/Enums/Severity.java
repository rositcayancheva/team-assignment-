package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums;

public enum Severity {
    Critical,
    Major,
    Minor

}
