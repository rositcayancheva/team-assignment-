package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl;

import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.ActivityHistoryImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Assignable;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class BugImpl extends WorkItemImpl implements Bug, Assignable {

    private List<String> stepsToReproduce;
    private Priority priority;
    private Severity severity;
    private Person assignee = new PersonImpl("No assignee");
    private boolean isAssigned;

    public BugImpl(String ID, String title, String description, String status, String stepsToReproduce, Priority priority, Severity severity) {

        super(ID, title, description, status);
        setStepsToReproduce(stepsToReproduce);
        setPriority(priority);
        setSeverity(severity);
        isAssigned = false;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public Person getAssignee() {
        if (!isAssigned){
            throw new IllegalArgumentException("The WorkItem is unassigned");
        }
        else
        return assignee;
    }

    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    public boolean isAssigned() {
        return isAssigned;
    }

    public void setAssigned(boolean assigned) {
        isAssigned = assigned;
    }

    @Override
    public void setStatus(String status) {
        String stat = status.toLowerCase();
        switch (stat) {
            case "active":
                this.status = "Active";
                break;
            case "fixed":
                this.status = "Fixed";
                break;
            default:
                throw new IllegalArgumentException("Bug status must be Active or Fixed");
        }
    }

    @Override
    protected String getHeader() {
        return "Bug ---";
    }

    @Override
    protected String getFooter() {
        return String.format("Priority: %s\n" +
                "Severity: %s", getPriority(), getSeverity());
    }

    private void setStepsToReproduce(String stepsToReproduce) {
        if (stepsToReproduce.isEmpty() || !(stepsToReproduce.contains("."))){throw new IllegalArgumentException("Steps to reproduce should not be empty!");}
        this.stepsToReproduce = Arrays.asList(stepsToReproduce.trim().split("."));
    }
}
