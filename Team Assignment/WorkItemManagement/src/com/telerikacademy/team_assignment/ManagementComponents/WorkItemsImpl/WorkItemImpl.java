package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl;

import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.ActivityHistoryImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemImpl implements WorkItem {
    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;

    private String ID;
    private String title;
    private String description;
    protected String status;
    private List<Comment> comments;
    private ActivityHistory activityHistory;
    private Board boardOfWorkItem;
    private Team teamOfWorkItem;

    protected WorkItemImpl(String ID, String title, String description, String status) {
        setID(ID);
        setTitle(title);
        setDescription(description);
        setStatus(status);
        comments = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
    }

    public String getID() {
        return ID;
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    public void addComment(String text, Person author) {
       if (text.isEmpty()){throw new IllegalArgumentException("Please enter comment.");}
       if (!(this.getTeamOfWorkItem().getMembers().contains(author))){throw new IllegalArgumentException("This person is not part of the team");}
        Comment comment = new Comment(text, author);
        comments.add(comment);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void addToActivityHistory(String string) {
        activityHistory.addToActivityHistory(string);
    }

    public ActivityHistory getActivityHistory() {
        return activityHistory;
    }

    public String getStatus() {
        return status;
    }

    public String toString() {
        return String.format("%s\n" +
                "Title: %s\n" +
                "ID: %s\n" +
                "Status: %s\n" +
                "%s", getHeader(), getTitle(), getID(), getStatus(), getFooter());
    }

    public Board getBoardOfWorkItem() {
        return boardOfWorkItem;
    }

    public Team getTeamOfWorkItem() {
        return teamOfWorkItem;
    }

    public void setBoardOfWorkItem(Board boardOfWorkItem) {
        this.boardOfWorkItem = boardOfWorkItem;
    }

    public void setTeamOfWorkItem(Team teamOfWorkItem) {
        this.teamOfWorkItem = teamOfWorkItem;
    }

    protected abstract void setStatus(String status);

    protected abstract String getHeader();

    protected abstract String getFooter();

    protected void setID(String ID) {
        if (ID.isEmpty()){throw new IllegalArgumentException("Enter ID!");}
        this.ID = ID;
    }

    protected void setTitle(String title) {

        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException("Title's length must be between 10 and 50 symbols");
        }
        this.title = title;
    }

    protected void setDescription(String description) {

        if (description.length() < MIN_DESCRIPTION_LENGTH || description.length() > MAX_DESCRIPTION_LENGTH) {
            throw new IllegalArgumentException("Description must be between 10 and 500 symbols");
        }
        this.description = description;
    }

}
