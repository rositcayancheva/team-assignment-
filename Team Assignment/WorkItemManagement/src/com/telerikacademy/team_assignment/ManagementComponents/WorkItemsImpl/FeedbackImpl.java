package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl;

import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.ActivityHistoryImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Feedback;

import java.util.List;

public class FeedbackImpl extends WorkItemImpl implements Feedback {
    private static final int MIN_RATING = -10;
    private static final int MAX_RATING = 10;

    private int rating;

    public FeedbackImpl(String ID, String title, String description, String status, int rating) {
        super(ID, title, description, status);
        setRating(rating);
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (rating < MIN_RATING || rating > MAX_RATING) {
            throw new IllegalArgumentException("Rating must be a number between -10 and 10");
        }
        this.rating = rating;
    }

    @Override
    public void setStatus(String status) {
        switch (status.toLowerCase()) {
            case "new":
                this.status = "New";
                break;
            case "unscheduled":
                this.status = "Unscheduled";
                break;
            case "scheduled":
                this.status = "Scheduled";
                break;
            case "done":
                this.status = "Done";
                break;
            default:
                throw new IllegalArgumentException("Bug status must be New, Unscheduled, Scheduled or Done");
        }
    }

    @Override
    protected String getHeader() {
        return "Feedback ---";
    }

    @Override
    protected String getFooter() {
        return String.format("Rating: %s", getRating());
    }
}