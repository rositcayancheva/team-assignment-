package com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Assignable;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;

import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;

import java.util.List;

public class StoryImpl extends WorkItemImpl implements Story, Assignable {
    private Priority priority;
    private StorySize size;
    private Person assignee = new PersonImpl("No assignee");
    private boolean isAssigned;

    public StoryImpl(String ID, String title, String description, String status, Priority priority, StorySize size) {

        super(ID, title, description, status);
        setPriority(priority);
        setSize(size);
        isAssigned = false;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    //SIZE
    @Override
    public StorySize getSize() {
        return size;
    }

    @Override
    public void setSize(StorySize size) {
        this.size = size;
    }

    @Override
    public Person getAssignee() {
        if (!isAssigned){
            throw new IllegalArgumentException("The WorkItem is unassigned");
        }
        return assignee;
    }

    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    public boolean isAssigned() {
        return isAssigned;
    }

    public void setAssigned(boolean assigned) {
        isAssigned = assigned;
    }

    @Override
    public void setStatus(String status) {
        switch(status.toLowerCase()){
            case "notdone": this.status = "NotDone"; break;
            case "inprogress": this.status = "InProgress"; break;
            case "done": this.status = "Done"; break;
            default: throw new IllegalArgumentException("Story status must be NotDone, InProgress or Done");
        }
    }

    @Override
    protected String getHeader() {
        return "Story ---";
    }

    @Override
    protected String getFooter() {
        return String.format("Priority: %s\n"+
                "Size: %s", getPriority(), getSize());
    }
}
