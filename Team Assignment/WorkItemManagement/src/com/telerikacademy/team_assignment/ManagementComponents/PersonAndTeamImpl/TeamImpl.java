package com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl;

import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.ActivityHistoryImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

public class TeamImpl implements Team {

    private String name;
    private List<Person> members;
    private HashMap<String, Board> boards;
    private ActivityHistory activityHistory;

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new HashMap<>();
        activityHistory = new ActivityHistoryImpl();
    }

    private void setName(String name) {
        if (!isNameValid.test(name)) {
            throw new IllegalArgumentException("Name should be between 5 and 15 symbols.");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addPersonToTeam(Person person) {
        members.add(person);
    }

    public void addBoardToTeam(String nameOfBoard, Board board) {
        boards.put(board.getName(), board);
    }

    public List<Person> getMembers() {
        return new ArrayList<>(members);
    }

    public HashMap<String, Board> getHashMapBoards() {
        return new HashMap<>(boards);
    }

    public List<Board> getBoards() {
        Collection<Board> values = boards.values();
        ArrayList<Board> arrayListofBoards = new ArrayList<Board>(values);
        return arrayListofBoards;
    }

    @Override
    public void addToActivityHistory(String string) {
        activityHistory.addToActivityHistory(string);
    }

    @Override
    public ActivityHistory getActivityHistory() {
        return activityHistory;
    }

    public String toString() {
        return getName();
    }

    private Predicate<String> isNameValid = (s) -> s.length() >= MIN_NAME_LEN && s.length() <= MAX_NAME_LEN;

}
