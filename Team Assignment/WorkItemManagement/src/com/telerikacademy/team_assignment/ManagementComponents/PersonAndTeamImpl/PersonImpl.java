package com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl;

import com.telerikacademy.team_assignment.ManagementComponents.AdditionalElements.ActivityHistoryImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.ActivityHistory;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PersonImpl implements Person {

    private String name;
    private List<WorkItem> workItems;
    private ActivityHistory activityHistory;

    public PersonImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
    }

    private void setName(String name) {
        if (!isNameValid.test(name)) {
            throw new IllegalArgumentException("Name should be between 5 and 15 symbols");
        }
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void addWorkItem(WorkItem workItem) {
        workItems.add(workItem);
    }

    public void removeWorkItem(WorkItem workItem) {
        workItems.remove(workItem);
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    @Override
    public void addToActivityHistory(String string) {
        activityHistory.addToActivityHistory(string);
    }

    public ActivityHistory getActivityHistory() {
        return activityHistory;
    }

    @Override
    public String toString() {
        return getName();
    }

    private Predicate<String> isNameValid = (s) -> s.length() >= MIN_NAME_LEN && s.length() <= MAX_NAME_LEN;
}
