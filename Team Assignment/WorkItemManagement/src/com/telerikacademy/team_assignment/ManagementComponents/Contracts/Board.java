package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import java.util.List;

public interface Board extends ActivityHistory {

    String getName();

    List<WorkItem> getListWorkItem();

    void addWorkItem(WorkItem item);

    String toString();
}
