package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import java.util.HashMap;
import java.util.List;

public interface Team extends ActivityHistory {

    int MIN_NAME_LEN = 5;
    int MAX_NAME_LEN = 15;

    String getName();

    void addPersonToTeam(Person person);

    void addBoardToTeam(String nameofBoard, Board board);

    List<Person> getMembers();

    List<Board> getBoards();

    HashMap<String, Board> getHashMapBoards();
}
