package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

public interface ActivityHistory {

    void addToActivityHistory(String string);

    ActivityHistory getActivityHistory();

}
