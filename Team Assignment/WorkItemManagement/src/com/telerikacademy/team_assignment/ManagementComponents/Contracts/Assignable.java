package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

public interface Assignable {
    Person getAssignee();
    void setAssigned(boolean assigned);
    void setAssignee(Person assignee);
    boolean isAssigned();
}
