package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Comment;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;

import java.util.List;

public interface WorkItem extends ActivityHistory {
    String getID();
    String getTitle();
    String getStatus();
    String getDescription();
    List<Comment> getComments();
    void addComment (String text, Person author);

    void setBoardOfWorkItem(Board boardOfWorkItem);
    void setTeamOfWorkItem(Team teamOfWorkItem);
    Board getBoardOfWorkItem();
    Team getTeamOfWorkItem();
}