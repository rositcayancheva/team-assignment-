package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;

public interface Story extends WorkItem {

    Priority getPriority();
    StorySize getSize();
    void setPriority(Priority priority);
    void setSize(StorySize size);
    void setStatus(String status);
    Person getAssignee();

}
