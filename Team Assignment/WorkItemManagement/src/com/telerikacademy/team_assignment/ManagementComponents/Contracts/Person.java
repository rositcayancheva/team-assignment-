package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import java.util.List;

public interface Person extends ActivityHistory {

    int MIN_NAME_LEN = 5;
    int MAX_NAME_LEN = 15;

    String getName();

    List<WorkItem> getWorkItems();

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);

    ActivityHistory getActivityHistory();


}
