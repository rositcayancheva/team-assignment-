package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;

import java.util.List;

public interface Bug extends WorkItem{
    List<String> getStepsToReproduce();
    Priority getPriority();
    Severity getSeverity();
    Person getAssignee();
    void setPriority(Priority priority);
    void setSeverity(Severity severity);
    void setStatus(String status);

}
