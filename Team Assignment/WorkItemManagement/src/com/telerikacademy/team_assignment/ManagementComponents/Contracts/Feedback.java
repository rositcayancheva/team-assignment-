package com.telerikacademy.team_assignment.ManagementComponents.Contracts;

public interface Feedback extends WorkItem {
    int getRating();
    void setStatus(String status);
    void setRating(int rating);
}
