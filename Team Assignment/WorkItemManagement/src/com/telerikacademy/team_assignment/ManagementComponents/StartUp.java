package com.telerikacademy.team_assignment.ManagementComponents;


import com.telerikacademy.team_assignment.core.EngineImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactoryImpl;

public class StartUp {

        public static void main(String[] args) {

            WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
            Engine engine = new EngineImpl(factory);
            engine.start();
        }

    }


