package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListAllTeamMembersCommand extends CommandImpl implements Command {

    private String nameOfTeam;
    private Team team;

    public ListAllTeamMembersCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        try {
            nameOfTeam = String.valueOf(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse List All TeamMembers command parameters.");
        }
        this.checkIfTeamExists(nameOfTeam);
        return listAllTeamMembers();
    }

    private String listAllTeamMembers() {
        team = engine.getHashMapTeams().get(nameOfTeam);
        if (team.getMembers().isEmpty()) {
            return "No members in this team";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Team consists of:\n");
        team.getMembers().stream().forEach(person -> stringBuilder.append(person.getName() + "\n"));

        return stringBuilder.toString();
    }
}
