package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeStoryStatus extends ChangeBase implements Command {

    private String status;
    private String text = "%s changed the status of story %s to %s";

    public ChangeStoryStatus(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            status = parameters.get(0);
            switchStatus(status);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeStoryStatus command parameters.");
        }
        change();

        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validation();
        ((Story) workItem).setStatus(status);
        addingActivityHistoryMessage(text);
    }

    private void validation() {
        if (!(workItem instanceof Story)) {
            throw new IllegalArgumentException(String.format("The work item with ID %s is not a Story", workItem.getID()));
        }
    }

    @Override
    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text, person.getName(), workItem.getID(), status));
    }

    private void switchStatus(String inputStatus) {
        String stat = inputStatus.toLowerCase();
        switch (stat) {
            case "notdone":
                status = "NotDone";
                break;
            case "inprogress":
                status = "InProgress";
                break;
            case "done":
                status = "Done";
                break;
            default:
                throw new IllegalArgumentException("Story status must be NotDone, InProgress or Done");
        }
    }
}
