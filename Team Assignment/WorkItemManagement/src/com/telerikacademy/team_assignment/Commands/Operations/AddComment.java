package com.telerikacademy.team_assignment.Commands.Operations;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;
import java.util.Scanner;

public class AddComment extends CommandImpl implements Command {

    private String nameOfPerson;
    private String workItemID;
    private String textOfomment;
    private Person person;
    private WorkItem workItem;
    private String text = "%s added a comment to Workitem with ID %s.";

    public AddComment(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            nameOfPerson = String.valueOf(parameters.get(0));
            workItemID = String.valueOf(parameters.get(1));
            textOfomment = String.valueOf(parameters.get(2));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddComment command parameters.");
        }

        addingComment();

        return String.format(text, nameOfPerson, workItemID);
    }

    private void addingComment() {
        validation();
        workItem.addComment(textOfomment, person);
       addToActivityHistory();
    }

    private void validation() {
        checkIfPersonExists(nameOfPerson);
        checkIfWorkItemExists(workItemID);
        if (textOfomment.isEmpty()) {
            throw new IllegalArgumentException("Comment cannot be empty");
        }
        person = engine.getHashMapPeople().get(nameOfPerson);
        workItem = engine.getHashMapWorkItems().get(workItemID);
    }

    private void addToActivityHistory(){
        person.addToActivityHistory(String.format(text, nameOfPerson, workItemID));
        workItem.addToActivityHistory(String.format(text, nameOfPerson, workItemID));
        workItem.getBoardOfWorkItem().addToActivityHistory(String.format(text, nameOfPerson, workItemID));
    }

}
