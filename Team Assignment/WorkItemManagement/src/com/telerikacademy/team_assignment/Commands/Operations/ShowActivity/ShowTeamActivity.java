package com.telerikacademy.team_assignment.Commands.Operations.ShowActivity;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ShowTeamActivity extends CommandImpl implements Command {

    public ShowTeamActivity(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        String nameOfTeam;

        try {
            nameOfTeam = String.valueOf(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowTeamActivity command parameters.");
        }

        this.checkIfTeamExists(nameOfTeam);

        Team team = engine.getHashMapTeams().get(nameOfTeam);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Activity history of %s\n",nameOfTeam));
        stringBuilder.append(team.getActivityHistory().toString());

        return stringBuilder.toString().trim();

    }
}
