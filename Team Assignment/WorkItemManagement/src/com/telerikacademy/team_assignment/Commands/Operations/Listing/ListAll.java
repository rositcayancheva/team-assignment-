package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ListAll extends CommandImpl implements Command {

    public ListAll(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        String whatWorkItemToList;
        try {
            whatWorkItemToList = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse 'List All' command parameters");
        }

        List<WorkItem> workItemsToList = new ArrayList<>(engine.getHashMapWorkItems().values());
        StringBuilder itemList = new StringBuilder();

        switch (whatWorkItemToList.toLowerCase()) {

            case "bugs":
                workItemsToList.stream().filter(filterBugs).forEach(p -> itemList.append(p.toString() + "\n\n"));
                break;
            case "stories":
                workItemsToList.stream().filter(filterStories).forEach(p -> itemList.append(p.toString() + "\n\n"));
                break;
            case "feedbacks":
                workItemsToList.stream().filter(filterFeedbacks).forEach(p -> itemList.append(p.toString() + "\n\n"));
                break;
            case "workitems":
                workItemsToList.stream().sorted(Comparator.comparing(WorkItem::getTitle)).
                        forEach(p -> itemList.append(p.toString() + "\n\n"));
                break;
            default:
                throw new IllegalArgumentException("Incompatible type");
        }

        if (itemList.length() == 0) {
            return (String.format("There are no %s to list", whatWorkItemToList));
        }

        return itemList.toString();
    }



}