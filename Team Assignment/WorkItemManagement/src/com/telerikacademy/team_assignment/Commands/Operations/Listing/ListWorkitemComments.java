package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListWorkitemComments extends CommandImpl implements Command {

    private String workItemId;
    private WorkItem workItem;

    public ListWorkitemComments(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            workItemId = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListWorkitemComments command");
        }

        validation();

        return showComments();
    }

   private String showComments(){
     if (workItem.getComments().isEmpty()){return "No comments to show";}

     StringBuilder stringBuilder = new StringBuilder();
       workItem.getComments().stream()
               .forEach(comment -> stringBuilder.append
                       (String.format("Comment: %s, added by %s \n", comment.getMessage(), comment.getAuthor())));
     return stringBuilder.toString().trim();
   }

   private void validation(){
       checkIfWorkItemExists(workItemId);
       workItem = engine.getHashMapWorkItems().get(workItemId);
   }

}
