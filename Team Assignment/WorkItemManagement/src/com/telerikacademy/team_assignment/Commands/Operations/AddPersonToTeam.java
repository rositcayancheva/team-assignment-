package com.telerikacademy.team_assignment.Commands.Operations;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class AddPersonToTeam extends CommandImpl implements Command {

    private String nameOfTeam;
    private String nameOfPerson;
    private Team team;
    private Person person;

    public AddPersonToTeam(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            nameOfPerson = String.valueOf(parameters.get(0));
            nameOfTeam = String.valueOf(parameters.get(1));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddPerson command parameters.");
        }
        addPersonToTeam();
        return String.format("%s joined team %s.", nameOfPerson, nameOfTeam);

    }

    private void addPersonToTeam() {
        validate();
        team.addPersonToTeam(person);
        team.addToActivityHistory(String.format("%s joined team %s.", nameOfPerson, nameOfTeam));
    }

    private void validate() {
        this.checkIfPersonExists(nameOfPerson);
        this.checkIfTeamExists(nameOfTeam);
        team = engine.getHashMapTeams().get(nameOfTeam);
        person = engine.getHashMapPeople().get(nameOfPerson);
        if (team.getMembers().contains(person)) {
            throw new IllegalArgumentException(String.format("%s is already a member of %s", nameOfPerson, nameOfTeam));
        }
    }
}
