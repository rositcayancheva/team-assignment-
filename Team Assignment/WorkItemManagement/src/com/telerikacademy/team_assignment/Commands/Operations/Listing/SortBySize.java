package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SortBySize extends CommandImpl implements Command {
    private StringBuilder sortedBySize;

    public SortBySize(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        sortedBySize = new StringBuilder();

        engine.getHashMapWorkItems().values()
                .stream()
                .filter(filterStories)
                .sorted(Comparator.comparing(item -> ((Story) item).getSize()))
                .forEach(item -> sortedBySize.append(item.toString() + "\n\n"));

        if (sortedBySize.length() == 0) {
            return "There are no created stories to list";
        }

        return sortedBySize.toString();
    }

    public String getSortedBySize() {
        if (sortedBySize.length() == 0) {
            sortedBySize.append("There are no created stories to list");
        }
        return sortedBySize.toString();
    }

    private Predicate<WorkItem> filterStories = (p -> p instanceof Story);

}
