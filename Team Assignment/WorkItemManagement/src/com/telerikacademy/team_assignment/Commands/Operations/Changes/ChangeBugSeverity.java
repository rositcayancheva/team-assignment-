package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.ParsingMethods;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeBugSeverity extends ChangeBase implements Command {

    private Severity severity;
    private String text = "%s changed the severity of bug %s to %s";

    public ChangeBugSeverity(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            severity =  ParsingMethods.parseStringToSeverity(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeBugSeverity command parameters.");
        }
        change();
        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validate();
        ((Bug) workItem).setSeverity(severity);
        addingActivityHistoryMessage(text);
    }

    protected void addingActivityHistoryMessage(String text) {
        super.addingActivityHistoryMessage(String.format(text,
                person.getName(), workItem.getID(), severity.toString()));
    }

    private void validate() {
        if (!(workItem instanceof Bug)) {
            throw new IllegalArgumentException("This work item is not a bug");
        }
    }
}

