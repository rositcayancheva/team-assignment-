package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListByStatus extends CommandImpl implements Command {

    private StringBuilder listedByStatus;
    private String whichStatusToList;

    public ListByStatus(WorkItemManagementFactory factory, Engine engine) {
       super(factory, engine);
    }
    @Override
    public String execute(List<String> parameters) {

        try {
            whichStatusToList = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse List by status command parameters");
        }
        checkIfTheStatusExists(whichStatusToList);

       listedByStatus = new StringBuilder();

        Predicate<WorkItem> filterByStatus =
                (p -> p.getStatus().equalsIgnoreCase(String.format("%s", whichStatusToList)));

        engine.getHashMapWorkItems().values().stream().
                filter(filterByStatus).
                forEach(p -> listedByStatus.append(p.toString() + "\n\n"));

        if (listedByStatus.length() == 0) {
            return String.format("There are no workitems with %s status to list", whichStatusToList);
        }

        return listedByStatus.toString();
    }
    public String getListedByStatus(){
        if (listedByStatus.length() == 0){
            listedByStatus.append(String.format("There are no workitems with %s status to list", whichStatusToList));
        }
        return listedByStatus.toString();
    }

    public String getWhichStatusToList() {
        return whichStatusToList;
    }
}
