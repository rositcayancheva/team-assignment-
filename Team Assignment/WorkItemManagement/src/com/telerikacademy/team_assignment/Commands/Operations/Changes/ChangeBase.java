package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public abstract class ChangeBase extends CommandImpl implements Command {

    private String workItemId;
    protected WorkItem workItem;
    private String personName;
    protected Person person;
    private String activityHistoryMessage;

    protected ChangeBase(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            workItemId = parameters.get(0);
            personName = parameters.get(1);

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse Change command parameters.");
        }

        validate();
        parameters.remove(1);
        parameters.remove(0);

        return "";
    }

    protected abstract void change ();

    protected String getActivityHistoryMessage() {
        return activityHistoryMessage;
    }

    protected void addingActivityHistoryMessage(String activityHistoryMessage){
        this.activityHistoryMessage = activityHistoryMessage;
        distributeToActivityHistory(activityHistoryMessage);
    }

    private void distributeToActivityHistory(String activityHistoryMessage) {
        engine.getHashMapWorkItems().get(workItemId).addToActivityHistory(activityHistoryMessage);
        engine.getHashMapPeople().get(personName).addToActivityHistory(activityHistoryMessage);
        workItem.getBoardOfWorkItem().addToActivityHistory(activityHistoryMessage);
    }

    private void validate() {
        checkIfPersonExists(personName);
        checkIfWorkItemExists(workItemId);
        workItem = engine.getHashMapWorkItems().get(workItemId);
        person = engine.getHashMapPeople().get(personName);
    }
}

