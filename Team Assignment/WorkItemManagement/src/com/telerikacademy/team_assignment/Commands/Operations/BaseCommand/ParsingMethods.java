package com.telerikacademy.team_assignment.Commands.Operations.BaseCommand;

import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;

public class ParsingMethods {

    public static Priority parseStringToPriority(String priority) {

        switch (priority.toLowerCase()) {

            case "high":
                return Priority.High;
            case "medium":
                return Priority.Medium;
            case "low":
                return Priority.Low;
            default:
                throw new IllegalArgumentException("Invalid priority");
        }
    }

    public static StorySize parseStringToSize(String size) {
        switch (size.toLowerCase()) {

            case "large":
                return StorySize.Large;
            case "medium":
                return StorySize.Medium;
            case "small":
                return StorySize.Small;
            default:
                throw new IllegalArgumentException("Invalid Story size");
        }
    }

    public static Severity parseStringToSeverity(String severity) {

        switch (severity.toLowerCase()) {
            case "critical":
                return Severity.Critical;
            case "major":
                return Severity.Major;
            case "minor":
                return Severity.Minor;
            default:
                throw new IllegalArgumentException("Invalid severity");
        }
    }
}
