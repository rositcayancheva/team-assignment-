package com.telerikacademy.team_assignment.Commands.Operations;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Assignable;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;


import java.util.List;

public class AssignWorkItem extends CommandImpl implements Command {
    private String personName;
    private String workItemId;
    private WorkItem item;
    private Person person;
    private String text = "Workitem with ID: %s was assigned to %s";

    public AssignWorkItem(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            personName = parameters.get(0);
            workItemId = parameters.get(1);

        } catch (Exception e) {

            throw new IllegalArgumentException("Failed to parse AssignWorkItem command parameters.");
        }

        assign(personName, workItemId);

        return String.format("WorkItem with ID %s was assigned to %s", workItemId, personName);
    }

    private void assign(String personName, String workItemId) {
        validation();
        person.addWorkItem(item);
        ((Assignable) item).setAssignee(person);
        ((Assignable) item).setAssigned(true);
        addToActivityHistory();
    }

    private void validation() {
        checkIfPersonExists(personName);
        checkIfWorkItemExists(workItemId);

        item = engine.getHashMapWorkItems().get(workItemId);
        person = engine.getHashMapPeople().get(personName);
        List<Person> teamMembers = item.getTeamOfWorkItem().getMembers();

        if (teamMembers.stream().noneMatch(member -> member.getName().equals(personName))) {
            throw new IllegalArgumentException(String.format("This person is not part of the team %s", item.getTeamOfWorkItem().getName()));
        }

        if (!(item instanceof Assignable)) {
            throw new IllegalArgumentException("The chosen work item is Feedback and cannot have assignee");
        }

        if (((Assignable) item).isAssigned()) {
            throw new IllegalArgumentException("This item is already assigned");
        }
    }

    private void addToActivityHistory() {
        String activityHistoryMessage = String.format(text, workItemId, personName);
        person.addToActivityHistory(activityHistoryMessage);
        item.addToActivityHistory(activityHistoryMessage);
        item.getTeamOfWorkItem().addToActivityHistory(activityHistoryMessage);
    }

}
