package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListByAssignee extends CommandImpl implements Command {

    private String assigneeName;
    private Person assignee;

    public ListByAssignee(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            assigneeName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse List By Assignee command!");
        }
        return listByAssignee();
    }

    private String listByAssignee() {
        validate();
        StringBuilder assigneeItems = new StringBuilder();
        assignee.getWorkItems().stream().forEach(p-> assigneeItems.append(p.toString() + "\n"));
        return assigneeItems.toString();
    }

    private void validate() {
        checkIfPersonExists(assigneeName);
        assignee = engine.getHashMapPeople().get(assigneeName);
    }
}
