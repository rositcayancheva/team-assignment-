package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.ParsingMethods;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeStorySize extends ChangeBase implements Command {

    private StorySize size;
    private String text = "%s changed the size of story %s to %s";

    public ChangeStorySize(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            size = ParsingMethods.parseStringToSize(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeStorySize command parameters.");
        }
        change();
        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validation();
        ((Story) workItem).setSize(size);
        addingActivityHistoryMessage(text);
    }

    private void validation() {
        if (!(workItem instanceof Story)) {
            throw new IllegalArgumentException(String.format("The work item with ID %s is not a Story", workItem.getID()));
        }
    }

    @Override
    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text, person.getName(), workItem.getID(), size));
    }
}
