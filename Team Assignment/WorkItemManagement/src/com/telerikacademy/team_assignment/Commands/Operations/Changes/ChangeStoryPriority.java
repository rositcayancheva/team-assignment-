package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeStoryPriority extends ChangeBase implements Command {

    private Priority priority;
    private String text = "%s changed the priority of story %s to %s";

    public ChangeStoryPriority(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            priority = Priority.valueOf(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeBugPriority command parameters.");
        }
        change();
        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validate();
        ((Story) workItem).setPriority(priority);
        addingActivityHistoryMessage(text);
    }

    @Override
    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text, person.getName(), workItem.getID(), priority.toString()));
    }

    private void validate() {
        if (!(workItem instanceof Story)) {
            throw new IllegalArgumentException(String.format("The work item with ID %s is not a Story", workItem.getID()));
        }
    }
}

