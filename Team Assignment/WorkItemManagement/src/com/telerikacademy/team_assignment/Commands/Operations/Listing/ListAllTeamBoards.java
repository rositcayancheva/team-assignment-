package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListAllTeamBoards extends CommandImpl implements Command {

    private  String nameOfTeam;
    private Team team;

    public ListAllTeamBoards(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        try {
            nameOfTeam = String.valueOf(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListAllTeamBoards command parameters.");
        }
        this.checkIfTeamExists(nameOfTeam);
        return listTeamBoards();
    }

    private String listTeamBoards (){

        team = engine.getHashMapTeams().get(nameOfTeam);
        if (team.getBoards().isEmpty()) { return "No boards in this team"; }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Boards of team %s\n", nameOfTeam));
        team.getBoards().stream().forEach(board -> stringBuilder.append(board.getName() + "\n"));

        return stringBuilder.toString();
    }
}

