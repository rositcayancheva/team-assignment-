package com.telerikacademy.team_assignment.Commands.Operations;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Assignable;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.ManagementComponents.PersonAndTeamImpl.PersonImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class UnassignWorkItem extends CommandImpl implements Command {

    private String personName;
    private String workItemId;
    private Person person;
    private WorkItem workItem;


    public UnassignWorkItem(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        try {
            workItemId = parameters.get(0);

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse 'Unassign work item' command parameters.");
        }

        unassign();

        return String.format("The work item with ID %s has been unassigned!", engine.getHashMapWorkItems().get(workItemId).getID());
    }

    private void unassign() {
        validate();
        person.removeWorkItem(workItem);
        ((Assignable) workItem).setAssigned(false);
        ((Assignable) workItem).setAssignee(new PersonImpl("No assignee"));
        addToActivityHistory();
    }


    private void validate() {
        checkIfWorkItemExists(workItemId);
        workItem = engine.getHashMapWorkItems().get(workItemId);
        if (!(workItem instanceof Assignable)) {
            throw new IllegalArgumentException("The work item cannot be assigned and unassigned!");
        }
        if (!(((Assignable) workItem).isAssigned())) {
            throw new IllegalArgumentException("This item has no assignee");
        }
        person = ((Assignable) workItem).getAssignee();
    }

    private void addToActivityHistory() {
        String text = "Workitem with ID: %s was unassigned to %s";
        String activityHistoryMessage = String.format(text, workItemId, person.getName());
        person.addToActivityHistory(activityHistoryMessage);
        workItem.addToActivityHistory(activityHistoryMessage);
        workItem.getTeamOfWorkItem().addToActivityHistory(activityHistoryMessage);
    }

}
