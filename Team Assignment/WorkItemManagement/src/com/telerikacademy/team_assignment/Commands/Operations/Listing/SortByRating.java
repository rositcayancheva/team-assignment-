package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Feedback;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SortByRating extends CommandImpl implements Command {

    private StringBuilder sortedByRating;
    public SortByRating(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        sortedByRating = new StringBuilder();

        engine.getHashMapWorkItems().values()
                .stream()
                .filter(filterFeedbacks)
                .sorted(sortByRating)
                .forEach(item -> sortedByRating.append(item.toString() + "\n\n"));

        if (sortedByRating.length() == 0) {
            return "There are no WorkItems to list";
        }

        return sortedByRating.toString();
    }

    public String getSortedByRating() {
        if (sortedByRating.length() == 0) {
            sortedByRating.append("There are no WorkItems to list");
        }
        return sortedByRating.toString();
    }

    private Predicate<WorkItem> filterFeedbacks = (p -> p instanceof Feedback);


}
