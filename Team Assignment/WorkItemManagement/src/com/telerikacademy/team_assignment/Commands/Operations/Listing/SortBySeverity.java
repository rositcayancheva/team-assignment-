package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.Comparator;
import java.util.List;


public class SortBySeverity extends CommandImpl implements Command {
    private StringBuilder sortedBySeverity;

    public SortBySeverity(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        sortedBySeverity = new StringBuilder();

        engine.getHashMapWorkItems().values().stream()
                .filter(filterBugs)
                .sorted(Comparator.comparing(item -> ((Bug) item).getSeverity()))
                .forEach(item -> sortedBySeverity.append(item.toString() + "\n\n"));

        if (sortedBySeverity.length() == 0) {
            return "There are no created stories to list";
        }

        return sortedBySeverity.toString();
    }

    public String getSortedBySeverity() {
        if (sortedBySeverity.length() == 0) {
            sortedBySeverity.append("There are no created stories to list");
        }
        return sortedBySeverity.toString();
    }

}
