package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListAllTeams extends CommandImpl implements Command {

    public ListAllTeams(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        return listAllTeams();
    }

    private String listAllTeams() {
        if (engine.getHashMapTeams().isEmpty()) {
            return "No teams to show";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Teams created:\n");
        engine.getHashMapTeams().forEach((name, team) -> stringBuilder.append(name + "\n"));
        return stringBuilder.toString();
    }
}
