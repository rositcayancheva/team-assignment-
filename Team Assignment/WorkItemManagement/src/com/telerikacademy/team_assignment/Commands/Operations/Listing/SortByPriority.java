package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import java.util.*;
import java.util.stream.Collectors;

public class SortByPriority extends CommandImpl implements Command {

    public SortByPriority(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    private StringBuilder sortedByPriority;

    @Override
    public String execute(List<String> parameters) {

        sortedByPriority = new StringBuilder();

        List<WorkItem> onlyBugs = engine.getHashMapWorkItems().values()
                .stream()
                .filter(filterBugs)
                .sorted(Comparator.comparing(item -> ((Bug) item).getPriority()))
                .collect(Collectors.toList());

        List<WorkItem> onlyStories = engine.getHashMapWorkItems().values()
                .stream()
                .filter(filterStories)
                .sorted(Comparator.comparing(item -> ((Story) item).getPriority()))
                .collect(Collectors.toList());

        if (!onlyBugs.isEmpty()) {
            sortedByPriority.append("Bugs: \n");
            onlyBugs.stream().forEach(p -> sortedByPriority.append(p.toString() + "\n\n"));
        }
        if (!onlyStories.isEmpty()) {
            sortedByPriority.append("Stories: \n");
            onlyStories.stream().forEach(p -> sortedByPriority.append(p.toString() + "\n\n"));
        }
        if (sortedByPriority.length() == 0) {
            return ("There are no WorkItems to list");
        }
        return sortedByPriority.toString();
    }

    public String getSortedByPriority() {
        if (sortedByPriority.length() == 0) {
            sortedByPriority.append("There are no WorkItems to list");
        }
        return sortedByPriority.toString();
    }
}
