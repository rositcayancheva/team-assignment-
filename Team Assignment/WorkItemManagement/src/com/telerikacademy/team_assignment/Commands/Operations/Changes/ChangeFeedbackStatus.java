package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Feedback;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeFeedbackStatus extends ChangeBase implements Command {

    private String status;
    private String text = "%s changed the status of feedback %s to %s";

    public ChangeFeedbackStatus(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            status = parameters.get(0);
            switchStatus(status);

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeFeedbackStatus command parameters.");
        }
        change();
        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validation();
        ((Feedback) workItem).setStatus(status);
        addingActivityHistoryMessage(text);
    }

    @Override
    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text,
                person.getName(), workItem.getID(), status));
    }

    private void validation() {
        if (!(workItem instanceof Feedback)) {
            throw new IllegalArgumentException(String.format("The work item with ID %s is not a Feedback", workItem.getID()));
        }
    }

    private void switchStatus(String inputStatus) {

        switch (inputStatus.toLowerCase()) {
            case "new":
                status = "New";
                break;
            case "unscheduled":
                status = "Unscheduled";
                break;
            case "scheduled":
                status = "Scheduled";
                break;
            case "done":
                status = "Done";
                break;
            default:
                throw new IllegalArgumentException("Feedback status must be New, Unscheduled, Scheduled or Done");
        }
    }
    
}
