package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeBugStatus extends ChangeBase implements Command {

    private String status;
    private String text = "%s changed the status of bug %s to %s";
    public ChangeBugStatus(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            status = parameters.get(0);
            String statusToLower = status.toLowerCase();
            trySwitchStatus(statusToLower);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeBugStatus command parameters.");
        }
        change();
        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validate();
        ((Bug) workItem).setStatus(String.valueOf(status));
        addingActivityHistoryMessage(text);
    }

    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text,
                person.getName(), workItem.getID(), status));
    }

    private void validate() {
        if (!(workItem instanceof Bug)) {
            throw new IllegalArgumentException("The work item is not a bug");
        }
    }

    private void trySwitchStatus(String status) {
        switch (status.toLowerCase()) {
            case "active":
                this.status = "Active";
                break;
            case "fixed":
                this.status = "Fixed";
                break;
            default:
                throw new IllegalArgumentException("Bug status must be active or fixed");
        }
    }
}

