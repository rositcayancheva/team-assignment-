package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;
import java.util.List;

public class ListAllPeople extends CommandImpl implements Command {

    private StringBuilder stringBuilder = new StringBuilder();
    private String header = "People created so far:";

    public ListAllPeople(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        if (engine.getHashMapPeople().isEmpty()) {
            stringBuilder.append(NO_PEOPLE_TO_SHOW);
            return stringBuilder.toString();
        }

        stringBuilder.append(header + "\n");
        engine.getHashMapPeople().values().stream().forEach(person -> stringBuilder.append(person.toString() + "\n"));

        return stringBuilder.toString();
    }

    public StringBuilder getStringBuilder() {
        return stringBuilder;
    }
}
