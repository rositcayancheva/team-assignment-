package com.telerikacademy.team_assignment.Commands.Operations.Changes;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Feedback;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ChangeFeedbackRating extends ChangeBase implements Command {

    private int rating;
    private String text = "%s changed the rating of feedback %s to %d";

    public ChangeFeedbackRating(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        super.execute(parameters);
        try {
            rating = Integer.parseInt(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeFeedbackRating command parameters.");
        }

        change();

        return super.getActivityHistoryMessage();
    }

    protected void change() {
        validate();
        ((Feedback) workItem).setRating(rating);
        addingActivityHistoryMessage(text);
    }

    protected void addingActivityHistoryMessage(String activityHistoryMessage) {
        super.addingActivityHistoryMessage(String.format(text,
                person.getName(), workItem.getID(), rating));
    }

    private void validate() {
        if (!(workItem instanceof Feedback)) {
            throw new IllegalArgumentException("The item is not a feedback");
        }
    }


}
