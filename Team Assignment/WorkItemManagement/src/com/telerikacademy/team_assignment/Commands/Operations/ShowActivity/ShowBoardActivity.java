package com.telerikacademy.team_assignment.Commands.Operations.ShowActivity;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ShowBoardActivity extends CommandImpl implements Command {

    private String nameOfTeam;
    private String nameOfBoard;
    private Team team;
    private Board board;

    public ShowBoardActivity(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            nameOfTeam = String.valueOf(parameters.get(0));
            nameOfBoard = String.valueOf(parameters.get(1));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowBoardActivity command parameters.");
        }

        return getActivityHistory();
    }


    private String getActivityHistory() {
        validation();
        StringBuilder stringBuilder = new StringBuilder();

        if (board.getActivityHistory().toString().isEmpty()) {
            stringBuilder.append("No board activity to show.");
            return stringBuilder.toString();
        }

        stringBuilder.append(String.format("Activity history of board %s\n" + "%s",
                nameOfBoard, board.getActivityHistory().toString()).trim());

        return stringBuilder.toString();
    }

    private void validation() {
        this.checkIfTeamExists(nameOfTeam);
        this.checkIfBoardExists(nameOfTeam, nameOfBoard);
        team = engine.getHashMapTeams().get(nameOfTeam);
        board = team.getHashMapBoards().get(nameOfBoard);
    }

}
