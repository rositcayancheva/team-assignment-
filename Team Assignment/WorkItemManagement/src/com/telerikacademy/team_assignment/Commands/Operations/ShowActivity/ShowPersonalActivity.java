package com.telerikacademy.team_assignment.Commands.Operations.ShowActivity;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ShowPersonalActivity extends CommandImpl implements Command {

    public ShowPersonalActivity(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        String nameOfPerson;
        try {
            nameOfPerson = String.valueOf(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowPersonalActivity command parameters.");
        }

        this.checkIfPersonExists(nameOfPerson);

        Person person = engine.getHashMapPeople().get(nameOfPerson);

        return String.format("Activity history of %s:\n" +
                "%s", nameOfPerson, person.getActivityHistory().toString()).trim();

    }

}
