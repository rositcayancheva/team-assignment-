package com.telerikacademy.team_assignment.Commands.Operations.Listing;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.*;

public class SortByTitle extends CommandImpl implements Command {

    private StringBuilder sortedByTitle;

    public SortByTitle(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        sortedByTitle = new StringBuilder();

        engine.getHashMapWorkItems()
                .values()
                .stream()
                .sorted(Comparator.comparing(WorkItem::getTitle))
                .forEach(item -> sortedByTitle.append(item.toString() + "\n\n"));

        if (sortedByTitle.toString().isEmpty()) {
            return "No workitems to show!";
        }

        return sortedByTitle.toString();
    }

    public String getStringBuilder() {
        if (sortedByTitle.toString().isEmpty()) {
            return "No workitems to show!";
        }
        return sortedByTitle.toString();
    }

}
