package com.telerikacademy.team_assignment.Commands.Operations.BaseCommand;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.*;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public abstract class CommandImpl implements Command {

    protected final WorkItemManagementFactory factory;
    protected final Engine engine;

    public CommandImpl(WorkItemManagementFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public abstract String execute(List<String> parameters);

    //Validation
    protected void checkIfPersonExists(String nameOfPerson) {
        if (!engine.getHashMapPeople().containsKey(nameOfPerson)) {
            throw new IllegalArgumentException(THIS_PERSON_DOES_NOT_EXIST);
        }
    }

    protected void checkIfPersonNameIsUnique(String nameOfPerson) {
        if (engine.getHashMapPeople().containsKey(nameOfPerson)) {
            throw new IllegalArgumentException(PERSON_ALREADY_EXISTS);
        }
    }

    protected void checkIfNameOfBoardIsUnique(String nameOfTeam, String nameOfBoard) {
        if (engine.getHashMapTeams().get(nameOfTeam).getHashMapBoards().containsKey(nameOfBoard)) {
            throw new IllegalArgumentException(BOARD_ALREADY_CREATED);
        }
    }

    protected void checkIfTeamNameIsUnique(String nameOfTeam) {
        if (engine.getHashMapTeams().containsKey(nameOfTeam)) {
            throw new IllegalArgumentException(TEAM_ALREADY_EXISTS);
        }
    }

    protected void checkIfWorkItemIDIsUnique(String workItemId) {
        if (engine.getHashMapWorkItems().containsKey(workItemId)) {
            throw new IllegalArgumentException(THIS_WORKITEM_ALREADY_EXISTS);
        }
    }

    protected void checkIfTeamExists(String nameOfTeam) {
        if (!engine.getHashMapTeams().containsKey(nameOfTeam)) {
            throw new IllegalArgumentException(THIS_TEAM_DOES_NOT_EXIST);
        }
    }

    protected void checkIfBoardExists(String nameOfTeam, String nameOfBoard) {
        if (!engine.getHashMapTeams().get(nameOfTeam).getHashMapBoards().containsKey(nameOfBoard)) {
            throw new IllegalArgumentException(BOARD_DOESNT_EXIST);
        }
    }

    protected void checkIfWorkItemExists(String workItemID) {
        if (!engine.getHashMapWorkItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(THIS_WORKITEM_DOES_NOT_EXIST);
        }
    }

    protected void checkIfPersonIsMemberOfTeam(String teamName, String personName) {
        Team team = engine.getHashMapTeams().get(teamName);
        Person person = engine.getHashMapPeople().get(personName);
        if (!(team.getMembers().contains(person))) {
            throw new IllegalArgumentException(PERSON_IS_NOT_MEMBER_OF_TEAM);
        }
    }

    protected void checkIfTheStatusExists(String whichStatusToList) {
        String[] statusArr = {"active", "fixed", "notdone", "inprogress", "done",
                "new", "unscheduled", "scheduled", "done"};
        List<String> statusList = Arrays.asList(statusArr);
        if (statusList.stream().noneMatch(element -> element.equals(whichStatusToList.toLowerCase()))) {
            throw new IllegalArgumentException("There is no such type of status");
        }
    }

    protected Predicate<WorkItem> filterBugs = (p -> p instanceof Bug);
    protected Predicate<WorkItem> filterStories = (p -> p instanceof Story);
    protected Predicate<WorkItem> filterFeedbacks = (p -> p instanceof Feedback);
    protected Comparator<WorkItem> sortByRating = (item1, item2) ->
            ((Feedback) item1).getRating() > ((Feedback) item2).getRating() ? 1 : -1;


}