package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.ParsingMethods;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Story;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.StorySize;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreateStoryCommand extends CreateWorkItemCommand implements Command {

    private Priority priority;
    private StorySize size;

    public CreateStoryCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        super.execute(parameters);
        try {
            priority = ParsingMethods.parseStringToPriority(parameters.get(0));
            size = ParsingMethods.parseStringToSize(parameters.get(1));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateStory command parameters.");
        }

        creation();
        return super.getActivityHistoryMessage();
    }

    protected void creation() {

        Story story = factory.createStory(getID(), getTitle(), getDescription(), getStatus(), priority, size);
        engine.getHashMapWorkItems().put(getID(), story);
        getBoard().addWorkItem(story);
        story.setBoardOfWorkItem(getBoard());
        story.setTeamOfWorkItem(engine.getHashMapTeams().get(getTeamName()));
        addToActivityHistory();
    }

    private void addToActivityHistory() {
        String text = "%s created story with ID: %s in board %s.";
        setActivityHistoryMessage(String.format(text, getPersonName(), getID(), getBoardName()));
    }
}
