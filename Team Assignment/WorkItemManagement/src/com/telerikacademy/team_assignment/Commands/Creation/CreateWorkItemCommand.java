package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public abstract class CreateWorkItemCommand extends CommandImpl implements Command {

    private String teamName;
    private String boardName;
    private String personName;
    private String ID;
    private String title;
    private String description;
    private String status;
    private String activityHistoryMessage;


    protected CreateWorkItemCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try{
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            personName = parameters.get(2);
            ID = parameters.get(3);
            title = parameters.get(4);
            description = parameters.get(5);
            status = parameters.get(6);


        }catch (Exception e){
            throw new IllegalArgumentException("Failed to parse CreateWorkItem command parameters.");
        }

        validate();

        for (int i = 0; i < 7; i++) {
            parameters.remove(0);
        }

        return "";
    }
    protected abstract void creation();

    protected String getActivityHistoryMessage() {
        return activityHistoryMessage;
    }

    private void validate() {
        checkIfWorkItemIDIsUnique(ID);
        checkIfTeamExists(teamName);
        checkIfBoardExists(teamName, boardName);
        checkIfPersonExists(personName);
        checkIfPersonIsMemberOfTeam(teamName, personName);

    }

    protected void setActivityHistoryMessage(String activityHistoryMessage){
        this.activityHistoryMessage = activityHistoryMessage;
        addToActivityHistory(this.activityHistoryMessage);
    }

    private void addToActivityHistory(String activityHistoryMessage) {
        engine.getHashMapWorkItems().get(ID).addToActivityHistory(activityHistoryMessage);
        engine.getHashMapPeople().get(personName).addToActivityHistory(activityHistoryMessage);
        engine.getHashMapTeams().get(teamName).
                getHashMapBoards().get(boardName).
                addToActivityHistory(activityHistoryMessage);
    }

    protected String getID() {
        return ID;
    }

    protected String getTitle() {
        return title;
    }

    protected String getDescription() {
        return description;
    }

    protected String getStatus() {
        return status;
    }

    protected String getTeamName() {
        return teamName;
    }

    protected String getBoardName() {
        return boardName;
    }

    protected String getPersonName() {
        return personName;
    }

    protected Board getBoard() {
        return engine.getHashMapTeams().get(teamName).getHashMapBoards().get(boardName);
    }
}

