package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Feedback;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreateFeedbackCommand extends CreateWorkItemCommand implements Command {

    private int rating;

    public CreateFeedbackCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        super.execute(parameters);
        try {
            rating = Integer.parseInt(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateFeedback command parameters.");
        }
        creation();
        return super.getActivityHistoryMessage();
    }

    protected void creation() {
        Feedback feedback = factory.createFeedback(getID(), getTitle(), getDescription(), getStatus(), rating);
        getBoard().addWorkItem(feedback);
        feedback.setBoardOfWorkItem(getBoard());
        engine.getHashMapWorkItems().put(getID(), feedback);

        feedback.setBoardOfWorkItem(getBoard());
        feedback.setTeamOfWorkItem(engine.getHashMapTeams().get(getTeamName()));
        addToActivityHistory();
    }

    private void addToActivityHistory() {
        String text = "%s created feedback with ID: %s in board %s.";
        setActivityHistoryMessage(String.format(text, getPersonName(), getID(), getBoardName()));
    }
}
