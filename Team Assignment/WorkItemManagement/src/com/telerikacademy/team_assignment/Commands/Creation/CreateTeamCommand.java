package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreateTeamCommand extends CommandImpl implements Command {

    private String name;
    private Team team;

    public CreateTeamCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    public String execute(List<String> parameters) {

        try {
            name = String.valueOf(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateTeam command parameters.");
        }
        creation();
        return String.format("Team with name %s was created.", name);
    }

    private void creation() {
        validation();
        team = factory.createTeam(name);
        engine.getHashMapTeams().put(name, team);
        addToActivityHistory();
    }

    private void validation() {
        this.checkIfTeamNameIsUnique(name);
    }


    private void addToActivityHistory() {
        String activityHistoryMessage = "Team %s was created.";
        team.addToActivityHistory(String.format(activityHistoryMessage, name));
    }
}
