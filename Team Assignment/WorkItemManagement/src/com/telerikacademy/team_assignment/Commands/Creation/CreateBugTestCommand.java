package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.ParsingMethods;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Bug;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.WorkItem;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Priority;
import com.telerikacademy.team_assignment.ManagementComponents.WorkItemsImpl.Enums.Severity;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreateBugTestCommand extends CreateWorkItemCommand implements Command {

    private Priority priority;
    private Severity severity;
    private String stepsToReproduce;

    public CreateBugTestCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        super.execute(parameters);

        try {
            stepsToReproduce = parameters.get(0);
            priority = ParsingMethods.parseStringToPriority(parameters.get(1));
            severity = ParsingMethods.parseStringToSeverity(parameters.get(2));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateBug command parameters.");
        }
        creation();

        return super.getActivityHistoryMessage();
    }

    protected void creation() {
        Bug bug = factory.createBug(getID(), getTitle(), getDescription(), getStatus(), stepsToReproduce, priority, severity);
        engine.getHashMapWorkItems().put(getID(), bug);
        getBoard().addWorkItem(bug);
        bug.setBoardOfWorkItem(getBoard());
        bug.setTeamOfWorkItem(engine.getHashMapTeams().get(getTeamName()));
        addToActivityHistory();
    }

    private void addToActivityHistory() {
        String text = "%s created bug with ID: %s in board %s.";
        setActivityHistoryMessage(String.format(text, getPersonName(), getID(), getBoardName()));
    }
}
