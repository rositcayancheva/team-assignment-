package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Board;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Team;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreateBoardCommand extends CommandImpl implements Command {

    private String nameOfTeam;
    private String nameOfBoard;
    private Team team;
    private String activityHistoryMessage = "Team %s created board %s.";

    public CreateBoardCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        try {
            nameOfTeam = String.valueOf(parameters.get(0));
            nameOfBoard = String.valueOf(parameters.get(1));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateBoard command parameters.");
        }
        creation();

        return (String.format(activityHistoryMessage, nameOfTeam, nameOfBoard));
    }

    private void creation() {
        validate();
        Board board = factory.createBoard(nameOfBoard);
        team = engine.getHashMapTeams().get(nameOfTeam);
        team.addBoardToTeam(nameOfBoard, board);
        addToActivityHistory();
    }

    private void validate() {
        this.checkIfTeamExists(nameOfTeam);
        this.checkIfNameOfBoardIsUnique(nameOfTeam, nameOfBoard);
    }

    private void addToActivityHistory() {
        team.addToActivityHistory(String.format(activityHistoryMessage, nameOfTeam, nameOfBoard));
    }

}
