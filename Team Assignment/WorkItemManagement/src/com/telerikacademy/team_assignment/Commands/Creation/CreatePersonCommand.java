package com.telerikacademy.team_assignment.Commands.Creation;

import com.telerikacademy.team_assignment.Commands.Operations.BaseCommand.CommandImpl;
import com.telerikacademy.team_assignment.Commands.Contracts.Command;
import com.telerikacademy.team_assignment.ManagementComponents.Contracts.Person;
import com.telerikacademy.team_assignment.core.contracts.Engine;
import com.telerikacademy.team_assignment.core.factories.WorkItemManagementFactory;

import java.util.List;

public class CreatePersonCommand extends CommandImpl implements Command {

    private String name;

    public CreatePersonCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    public String execute(List<String> parameters) {

        try {
            name = String.valueOf(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreatePerson command parameters.");
        }
        creation(name);
        return String.format("Person with name %s was created.", name);
    }

    private void creation(String name) {
        validation();
        Person person = factory.createPerson(name);
        engine.getHashMapPeople().put(name, person);
    }

    private void validation() {
        this.checkIfPersonNameIsUnique(this.name);
    }

}