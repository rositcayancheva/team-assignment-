package com.telerikacademy.team_assignment.Commands.Contracts;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);

    String BOARD_DOESNT_EXIST = "This board does not exist!";
    String THIS_TEAM_DOES_NOT_EXIST = "This team does not exist!";
    String THIS_PERSON_DOES_NOT_EXIST = "This person does not exist!";
    String PERSON_ALREADY_EXISTS = "This person already exists!";
    String BOARD_ALREADY_CREATED = "This board was already created!";
    String THIS_WORKITEM_DOES_NOT_EXIST = "This WorkItem does not exist!";
    String THIS_WORKITEM_ALREADY_EXISTS = "This WorkItem already exists!";
    String TEAM_ALREADY_EXISTS = "This team already exists!";
    String NO_PEOPLE_TO_SHOW = "There are no people to show!";
    String PERSON_IS_NOT_MEMBER_OF_TEAM = "This person is not a team member!";
}